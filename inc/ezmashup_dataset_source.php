<?php
/**
 * Ce fichier contient les fonctions d'API des `datasets source` du plugin Mashup Factory.
 *
 * @package SPIP\EZMASHUP\SOURCE\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_EZMASHUP_COEFF_MAX_DISTANT')) {
	define('_EZMASHUP_COEFF_MAX_DISTANT', 5);
}

/**
 * Extrait le contenu de tous les datasets source basiques ou d'un dataset source addon donné.
 * Le contenu est renvoyé sous la forme d'une liste d'items qui seront traités par la suite pour être insérés dans
 * le dataset cible concerné.
 *
 * @api
 *
 * @param string     $id_source Identifiant d'une source addon donnée ou vide pour toutes les sources primaires.
 * @param array      $feed      Description complète du feed.
 * @param null|array $erreur    Tableau constitutif d'une erreur ou vide si aucune erreur à remonter
 *
 * @throws Exception
 *
 * @return array Tableau des items extraits de la source. Un tableau vide est constitutif d'une erreur.
 */
function dataset_source_extraire(string $id_source, array $feed, ?array &$erreur = []) : array {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Initialisation de l'erreur éventuelle et d'un indicateur permettant de savoir si l'erreur doit être logée
	// ou si elle a déjà été logée dans une fonction appelée
	$erreur = [];
	$loger_erreur = false;

	// Inclure les services de Mashup Factory
	include_spip('ezmashup/ezmashup');

	// Initialisation des données de sortie
	$contenu = [];

	// Suivant qu'on demande toutes les sources primaires ou une source addon donnée on constitue la liste des
	// sources à extraire
	if ($id_source) {
		$sources = [$id_source => $feed['sources_addon'][$id_source]];
		$type_source = 'sources_addon';
	} else {
		$sources = $feed['sources_basic'];
		$type_source = 'sources_basic';
	}
	// On boucle sur toutes les sources primaires
	foreach ($sources as $_id_source => $_source) {
		// Acquisition du contenu décodé en provenance de la source
		$contenu_source = [];
		if ($_source['source']['type'] === 'web') {
			// Acquisition de la page ciblée par son url
			$reponse = dataset_source_requeter($_source['source']['uri'], [], $erreur);
			if ($reponse) {
				$contenu_source = dataset_source_decoder($reponse, $type_source, $_id_source, $feed, $erreur);
			}
		} elseif ($_source['source']['type'] === 'api') {
			// Acquisition des données de l'api par son url
			include_spip('inc/distant');
			$options = [
				'transcoder' => true,
				'taille_max' => _INC_DISTANT_MAX_SIZE * _EZMASHUP_COEFF_MAX_DISTANT,
			];
			$reponse = dataset_source_requeter($_source['source']['uri'], $options, $erreur);
			if ($reponse) {
				$contenu_source = dataset_source_decoder($reponse, $type_source, $_id_source, $feed, $erreur);
			}
		} elseif ($_source['source']['type'] === 'file') {
			// Vérification de l'existence du fichier source : celui-ci est localisé dans le dossier du feed
			// - soit dans le plugin
			// - soit dans _DIR_ETC car il a été créé par formulaire via l'api de Cache Factory
			// On teste les deux cas un après l'autre
			$dossier = ezmashup_feed_initialiser_dossier($feed['plugin']);
			$fichier = find_in_path($dossier . $feed['feed_id'] . '/' . $_source['source']['uri']);
			if (!$fichier) {
				// Le type de cache dépend du format du fichier
				include_spip('inc/ezcache_cache');
				$type_cache = 'source_' . $_source['source']['format'];

				// L'identifiant canonique du cache.
				$cache = [
					'sous_dossier' => "{$feed['plugin']}/" . $dossier . "{$feed['feed_id']}/",
					'id_source'    => $_id_source
				];
				$fichier = cache_est_valide('ezmashup', $type_cache, $cache);
			}
			if ($fichier) {
				// On lit le contenu du fichier et on le décode dans la foulée en utilisant le plugin Encoder Factory
				include_spip('inc/flock');
				lire_fichier($fichier, $contenu_brut);
				$contenu_source = dataset_source_decoder($contenu_brut, $type_source, $_id_source, $feed, $erreur);
			} else {
				// Erreur fichier source introuvable
				$erreur = [
					'level'      => _LOG_ERREUR,
					'code'       => 'erreur_source_file',
					'parameters' => [
						'type_source' => $type_source,
						'source'      => $_id_source,
						'uri'         => $_source['source']['uri'],
					]
				];
				$loger_erreur = true;
			}
		} else {
			// Erreur type de source invalide
			$erreur = [
				'level'      => _LOG_ERREUR,
				'code'       => 'erreur_source_extract_type',
				'parameters' => [
					'type_source' => $type_source,
					'source'      => $_id_source,
					'type'        => $_source['source']['type'],
				]
			];
			$loger_erreur = true;
		}

		// Ajouter le contenu de la source au contenu des sources précédentes si besoin
		if ($erreur) {
			// Si on a détectée une erreur on stoppe le traitement en cours
			break;
		} elseif ($contenu_source) {
			$contenu = array_merge($contenu, $contenu_source);
		}
	}

	// Si une erreur est détectée, on la loge si nouvelle et on met le contenu à retourner à vide
	if ($erreur) {
		$contenu = [];
		if ($loger_erreur) {
			$message = ezmashup_log_creer_message($erreur);
			spip_log($message, 'ezmashup' . $erreur['level']);
		}
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $contenu;
}

/**
 * Normalise la description d'un dataset source donné.
 * Il est possible de fournir le dataset par défaut qui servira à la normalisation (cas du dataset '*').
 *
 * @param array      $dataset Dataset à normaliser
 * @param null|array $options Options pour compléter la normalisation
 *                            - array `dataset_defaut`: Dataset par défaut à utiliser. Ce dataset doit déjà être normalisé
 *                            Si aucun dataset fourni, la fonction utilise le dataset par défaut de base où tous
 *                            les champs sont 'vides'.
 *                            - string `dossier_feed` : Chemin relatif du dossier de rangement du feed
 *                            - string `ìnclude_feed` : Chemin relatif de l'include des fonctions spécifiques du feed
 *
 * @return array Dataset normalisé
 */
function dataset_source_normaliser_configuration(array $dataset, ?array $options = []) : array {
	$dataset_defaut_base = [
		'source' => [
			'type'        => '',
			'format'      => '',
			'uri'         => '',
			'last_update' => '',
			'version'     => '',
			'license'     => ''
		],
		'decoding' => [],
		'provider' => [
			'name' => '',
			'url'  => '',
		],
		'build_from' => [
			'file' => '',
		]
	];

	// Si aucun dataset par défaut n'est fourni, on utilise celui de base.
	$dataset_defaut = !empty($options['dataset_defaut']) ? $options['dataset_defaut'] : $dataset_defaut_base;

	// On normalise chaque index de premier niveau (le dataset n'a toujours que 2 niveaux).
	foreach ($dataset_defaut as $_cle => $_element) {
		if (empty($dataset[$_cle])) {
			$dataset[$_cle] = $_element;
		} else {
			$dataset[$_cle] = array_merge($_element, $dataset[$_cle]);
		}

		// Traitement particulier de l'include de la fonction de décodage si elle existe
		if (
			($_cle === 'decoding')
			and (!empty($dataset[$_cle]['callback']))
		) {
			if (empty($dataset[$_cle]['include'])) {
				$dataset[$_cle]['include'] = $options['include_feed'] ?? '';
			} else {
				include_spip('ezmashup/ezmashup');
				$dataset[$_cle]['include'] = ezmashup_normaliser_include($dataset[$_cle]['include'], $options['dossier_feed']);
			}
		}
	}

	return $dataset;
}

/**
 * Renvoie, à partir de l'url fournie, le contenu brut récupéré d'une source de type API ou page web, ou une erreur sinon.
 *
 * @uses recuperer_url()
 *
 * @param string     $url     URL complète de la requête
 * @param array      $options Options de la requête
 *                            - bool `transcoder` : true si on veut transcoder la page dans le charset du site
 *                            - int  `taille_max` : Arrêter le contenu au-delà (0 = seulement les entetes ==> requête HEAD).
 *                            Par defaut taille_max = 1Mo ou 16Mo si copie dans un fichier
 * @param null|array $erreur  Tableau constitutif d'une erreur ou vide si aucune erreur à remonter
 *
 * @throws Exception
 *
 * @return string La chaine renvoyée en réponse à la requête ou vide si une erreur s'est produit.
 */
function dataset_source_requeter(string $url, array $options, ?array &$erreur = []) : string {
	// Initialisation de l'erreur éventuelle
	$erreur = [];

	// Acquisition du flux de données
	include_spip('inc/distant');
	$flux = recuperer_url($url, $options);

	// Initialisation du la réponse
	$reponse = '';

	if (
		!$flux
		or ($flux['status'] === 404)
		or empty($flux['page'])
	) {
		// Erreur serveur
		$erreur = [
			'level'      => _LOG_ERREUR,
			'code'       => 'erreur_source_extract_request',
			'parameters' => [
				'url' => $url,
			]
		];
	} else {
		$reponse = $flux['page'];
	}

	// Si une erreur est détectée, on la loge, la réponse retournée étant à vide
	if ($erreur) {
		include_spip('ezmashup/ezmashup');
		$message = ezmashup_log_creer_message($erreur);
		spip_log($message, 'ezmashup' . $erreur['level']);
	}

	return $reponse;
}

/**
 * Décode un contenu d'une source pour en extraire un tableau de données.
 * La fonction utilise l'API de décodage fournie par le plugin Encoder Factory.
 *
 * @param string     $contenu     Chaine extraite de la source à décoder selon le format et les paramètres associés.
 * @param string     $type_source Type de source basique (`sources_basic`) ou additionnelle (`sources_addon`).
 * @param string     $id_source   Identifiant de la source.
 * @param array      $feed        Description complète du feed.
 * @param null|array $erreur      Tableau constitutif d'une erreur ou vide si aucune erreur à remonter
 *
 * @throws Exception
 *
 * @return array Tableau des données résultant du décodage ou vide ce qui est constitutif d'une erreur.
 */
function dataset_source_decoder(string $contenu, string $type_source, string $id_source, array $feed, ?array &$erreur = []) : array {
	// Initialisation de l'erreur éventuelle
	$erreur = [];

	// Identification des options de décodage de la source si elles existent
	$parametres_decodage = [];
	if (!empty($feed[$type_source][$id_source]['decoding'])) {
		$parametres_decodage = $feed[$type_source][$id_source]['decoding'];
	}

	// Identification du format de la source
	$format = $feed[$type_source][$id_source]['source']['format'];

	// Initialisation du contenu décodé
	$contenu_decode = [];

	if (isset($parametres_decodage['callback'])) {
		// Si une fonction de décodage spécifique existe pour la source :
		// - une option définie l'include qui la contient
		// - elle contient toujours en argument le contenu sous forme de chaine
		// - il est possible de lui passer des paramètres suplémentaires sous forme d'un tableau d'options nommées
		include_spip($parametres_decodage['include']);
		$decoder = $parametres_decodage['callback'];
		$contenu_decode = $decoder($contenu, $parametres_decodage['arguments'] ?? [], $id_source, $feed);
		if (!$contenu_decode) {
			// Erreur décodage par la callback
			$erreur = [
				'level'      => _LOG_ERREUR,
				'code'       => 'erreur_source_extract_callback',
				'parameters' => [
					'type_source' => $type_source,
					'source'      => $id_source,
					'callback'    => $decoder
				]
			];
		}
	} else {
		// Init des options de décodage en fonction du format et du codage qui parfois ne coincide pas exactement
		// avec le format de ezcodec
		$codage = $format;
		$options = [];
		if ($format === 'xml') {
			$options['with_root'] = false;
		} elseif (in_array($format, ['json', 'geojson', 'topojson'])) {
			$codage = 'json';
		} elseif ($format === 'csv') {
			if (!empty($parametres_decodage['delimiter'])) {
				$options['delim'] = $parametres_decodage['delimiter'];
			}
		} else {
			// Erreur format invalide
			$erreur = [
				'level'      => _LOG_ERREUR,
				'code'       => 'erreur_source_extract_format',
				'parameters' => [
					'type_source' => $type_source,
					'source'      => $id_source,
					'format'      => $format,
				]
			];
		}

		// Décodage si le format est ok
		if (!$erreur) {
			include_spip('inc/ezcodec');
			$contenu_decode = contenu_decoder($contenu, $codage, $options);
			if (!$contenu_decode) {
				// Erreur décodage
				$erreur = [
					'level'      => _LOG_ERREUR,
					'code'       => 'erreur_source_extract',
					'parameters' => [
						'format'      => $format,
						'type_source' => $type_source,
						'source'      => $id_source,
					]
				];
			}
		}
	}

	if ($erreur) {
		// Si une erreur est détectée, on la loge et on s'assure que le contenu à retourner soit un tableau vide
		$contenu_decode = [];
		include_spip('ezmashup/ezmashup');
		$message = ezmashup_log_creer_message($erreur);
		spip_log($message, 'ezmashup' . $erreur['level']);
	} elseif (
		$contenu_decode
		and !empty($parametres_decodage['root_node'])
	) {
		// Si il existe une option pour extraire qu'une partie de la source décodée, on l'applique.
		include_spip('inc/filtres');
		$contenu_decode = table_valeur($contenu_decode, $parametres_decodage['root_node'], []);
	}

	return $contenu_decode;
}
