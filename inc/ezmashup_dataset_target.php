<?php
/**
 * Ce fichier contient les fonctions d'API des `datasets cible` du plugin Mashup Factory.
 *
 * @package SPIP\EZMASHUP\TARGET\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Constitue, à partir de l'ensemble des sources primaires du feed, un tableau des éléments
 * prêt à être inséré dans une table de la base de données.
 *
 * @api
 *
 * @param string     $plugin Préfixe du plugin utilisateur.
 * @param array      $feed   Description complète du feed.
 * @param null|array $erreur Tableau constitutif d'une erreur ou vide si aucune erreur à remonter
 *
 * @return array La liste des enregistrements à stocker dans la cible concernée ou vide si erreur
 * @throws Exception
 */
function dataset_target_peupler(string $plugin, array $feed, ?array &$erreur = []) : array {
	// Initialisation de l'erreur éventuelle et d'un indicateur permettant de savoir si l'erreur doit être logée
	// ou si elle a déjà été logée dans une fonction appelée
	$erreur = [];
	$loger_erreur = false;

	// Identifier le feed
	$id_feed = $feed['feed_id'];

	// Inclure les services de Mashup Factory
	include_spip('ezmashup/ezmashup');

	// Inclure les apis des sources et des enregistrements des cibles
	include_spip('inc/ezmashup_dataset_source');
	include_spip('inc/ezmashup_record');

	// Initialisations des enregistrements
	$enregistrements = [];

	// Détermination de la clé primaire
	$cle_primaire_table = $feed['target_options']['primary_key'];

	// Récupération du contenu de la source sous la forme d'un tableau de champs valorisés.
	$contenu_source = dataset_source_extraire('', $feed, $erreur);
	if ($contenu_source) {
		$liste_cles_primaires = [];
		foreach ($contenu_source as $_item) {
			// Il est possible que la source nécessite des traitements spécifiques via un service du plugin utilisateur.
			// Ce service peut soit compléter ou modifier l'item, soit l'exclure en renvoyant vide.
			$item_source = ezmashup_item_completer($plugin, $_item, $feed);
			if ($item_source) {
				// On remplit les champs basiques, extra et statiques de l'enregistrement à partir de la source et de la configuration.
				$enregistrement = record_remplir($item_source, $feed, $erreur);
				if ($enregistrement) {
					// Extraire et stocker la clé primaire de l'enregistrement
					$cle_primaire = [];
					foreach ($cle_primaire_table as $_champ_cle) {
						if (isset($enregistrement[$_champ_cle])) {
							$cle_primaire[$_champ_cle] = $enregistrement[$_champ_cle];
						} else {
							// Erreur dans la clé primaire ou l'enregistrement : on stoppe l'exécution.
							$cle_primaire = [];
							break;
						}
					}

					// On ajoute l'élément que si la clé primaire a bien été trouvée et si la valeur de cette clé n'est pas en
					// doublon avec un élément déjà enregistré.
					if ($cle_primaire) {
						// On tri la clé primaire et on la transforme en chaine pour la tester et la stocker
						ksort($cle_primaire);
						$cle_primaire = implode(',', $cle_primaire);
						$index_enregistrement = array_search($cle_primaire, $liste_cles_primaires);
						if ($index_enregistrement === false) {
							// On rajoute cette clé dans la liste
							$liste_cles_primaires[] = $cle_primaire;

							// On complète l'enregistrement avec les traductions, des éventuels traitements spécifiques au feed
							// et le retrait des champs inutiles pour le stockage
							$enregistrement = ezmashup_record_completer($plugin, $enregistrement, $feed);

							// Ajout de l'enregistrement finalisé dans la liste.
							$enregistrements[] = $enregistrement;
						} else {
							// Vérifier si l'enregistrement en cours de constitution doit être fusionné avec celui déjà dans la liste.
							// - si la fusion n'est pas possible c'est que l'on a à faire à un doublon
							$enregistrement_fusionne = ezmashup_record_fusionner(
								$plugin,
								$enregistrement,
								$enregistrements[$index_enregistrement],
								$feed
							);
							if ($enregistrement_fusionne) {
								$enregistrements[$index_enregistrement] = $enregistrement_fusionne;
							} else {
								// On trace la collision sans générer d'erreur
								$log = [
									'level'      => _LOG_AVERTISSEMENT,
									'code'       => 'erreur_target_record_exists',
									'parameters' => [
										'key'    => $cle_primaire,
										'feed'   => $id_feed,
										'record' => var_export($enregistrement, true),
									]
								];
								$message = ezmashup_log_creer_message($log);
								spip_log($message, 'ezmashup' . $log['level']);
							}
						}
					} else {
						// Erreur clé primaire non fournie pour l'enregistrement en cours
						// -> on stoppe le traitement
						$erreur = [
							'level'      => _LOG_ERREUR,
							'code'       => 'erreur_target_record_nokey',
							'parameters' => [
								'feed'   => $id_feed,
								'record' => var_export($enregistrement, true),
							]
						];
						$loger_erreur = true;
						break;
					}
				} else {
					// Le remplissage de l'enregistrement s'est mal passé : on stoppe le traitement mais on ne
					// loge pas l'erreur ici car cela a déjà été fait par la fonction appelée
					break;
				}
			} else {
				// On trace l'exclusion sans générer d'erreur
				$log = [
					'level'      => _LOG_AVERTISSEMENT,
					'code'       => 'erreur_target_item_excluded',
					'parameters' => [
						'feed' => $id_feed,
						'item' => var_export($_item, true),
					]
				];
				$message = ezmashup_log_creer_message($log);
				spip_log($message, 'ezmashup' . $log['level']);
			}
		}
	}

	if ($erreur) {
		// Si une erreur est détectée, on la loge si nouvelle et on met les enregistrements à vide.
		$enregistrements = [];
		if ($loger_erreur) {
			$message = ezmashup_log_creer_message($erreur);
			spip_log($message, 'ezmashup' . $erreur['level']);
		}
	} elseif ($enregistrements) {
		// Si aucune erreur et qu'il existe des enregistrements, on les finalise
		$enregistrements = ezmashup_record_list_completer($plugin, $enregistrements, $feed);
	}

	return $enregistrements;
}

/**
 * Supprime, un dataset cible d'un feed donné.
 * La meta consignant les informations de peuplement est aussi effacée.
 *
 * @api
 *
 * @uses dataset_target_deconsigner()
 * @uses ezmashup_target_initialiser_dossier()
 *
 * @param string     $plugin Préfixe du plugin utilisateur.
 * @param array      $feed   Description complète du feed.
 * @param null|array $erreur Tableau constitutif d'une erreur ou vide si aucune erreur à remonter
 *
 * @return bool `true` si la suppression s'est bien passée, `false` sinon
 */
function dataset_target_supprimer(string $plugin, array $feed, ?array &$erreur = []) : bool {
	// Initialiser le retour de la fonction et l'eventuel bloc d'erreur
	$retour = true;
	$erreur = [];

	// Inclure les services de Mashup Factory
	include_spip('ezmashup/ezmashup');

	if ($feed['target_format'] === 'sql_table') {
		// On supprime les enregistrements associés au feed
		// -- si la table est utilisée pour plusieurs feeds, alors il est nécessaire de filtrer le vidage sur l'id du feed
		$where = [];
		if ($feed['target_options']['feed_column']) {
			$where = [
				$feed['target_options']['feed_column'] . '=' . sql_quote($feed['feed_id'])
			];
		}
		$table = $feed['target_id'];
		if (sql_delete($table, $where) === false) {
			// Erreur de suppression en base de données de la cible
			$erreur = [
				'level'      => _LOG_ERREUR,
				'code'       => 'erreur_target_delete_sql',
				'parameters' => [
					'target' => $table,
					'feed'   => $feed['feed_id']
				]
			];
		}
	} elseif ($feed['target_format'] === 'file') {
		// On construit le chemin du fichier cible dans le path
		// On construit le chemin du fichier cible avec l'api de ezcache pour le type target
		include_spip('inc/ezcache_cache');
		$cache = [
			'sous_dossier' => $plugin . '/' . ezmashup_target_initialiser_dossier($plugin),
			'id_target'    => $feed['target_id']
		];
		if (
			cache_est_valide('ezmashup', 'target', $cache)
			and !cache_supprimer('ezmashup', 'target', $cache, false)
		) {
			// Erreur de suppression du fichier de la cible
			$erreur = [
				'level'      => _LOG_ERREUR,
				'code'       => 'erreur_target_delete_fichier',
				'parameters' => [
					'target' => $feed['target_id'],
					'feed'   => $feed['feed_id']
				]
			];
		}
	} else {
		// Erreur format de cible invalide
		$erreur = [
			'level'      => _LOG_ERREUR,
			'code'       => 'erreur_target_storage_format',
			'parameters' => [
				'format' => $feed['target_format'],
				'target' => $feed['target_id'],
				'feed'   => $feed['feed_id']
			]
		];
	}

	if ($erreur) {
		// Si une erreur est détectée, on la loge et on met le retour à false
		$retour = false;
		$message = ezmashup_log_creer_message($erreur);
		spip_log($message, 'ezmashup' . $erreur['level']);
	} else {
		// Si pas d'erreur de suppression on déconsigne la cible
		dataset_target_deconsigner($plugin, $feed['feed_id']);
	}

	return $retour;
}

/**
 * Stocke les enregistrements cible comme indiqué dans la configuration du feed, pour créer le dataset cible.
 *
 * @param string     $plugin          Préfixe du plugin utilisateur.
 * @param array      $enregistrements Liste des enregistrements prêts à être insérés dans la table concernée.
 * @param array      $feed            Description complète du feed.
 * @param null|array $erreur          Tableau constitutif d'une erreur ou vide si aucune erreur à remonter
 *
 * @return int Nombre d'enregistrements réellement stocké, si le stockage s'est bien passé, `0` sinon
 */
function dataset_target_stocker(string $plugin, array $enregistrements, array $feed, ?array &$erreur = []) : int {
	// Initialiser le retour de la fonction conformément à la configuration du feed
	$nb_insertions = 0;
	$erreur = [];

	// Inclure les services de Mashup Factory
	include_spip('ezmashup/ezmashup');

	if ($feed['target_format'] === 'sql_table') {
		// Identification de la table
		$table = $feed['target_id'];

		// Pour assurer un traitement cohérent on utilise les transactions
		if (sql_preferer_transaction()) {
			// Cas de Sqlite
			sql_demarrer_transaction();
		} else {
			// Cas de MySQL
			sql_query('START TRANSACTION');
		}

		// On insère les enregistrements dans la base
		if ($feed['target_options']['stop_on_error']) {
			// Si on stoppe sur la première erreur, c'est que l'on veut un contexte final cohérent sans erreur.
			// De fait, on optimise le stockage en faisant des insertions par ensemble d'enregistrements et dès la
			// première erreur on sort de la boucle.
			$max_chunk = $feed['target_options']['max_chunk'] === 0
				? 50
				: min($feed['target_options']['max_chunk'], 50);
			foreach (array_chunk($enregistrements, $max_chunk) as $_enregistrements) {
				if (sql_insertq_multi($table, $_enregistrements) === false) {
					// Erreur d'insertion en base de données de la cible : on stoppe le traitement
					$erreur = [
						'level'      => _LOG_ERREUR,
						'code'       => 'erreur_target_storage_sql',
						'parameters' => [
							'target' => $table,
							'feed'   => $feed['feed_id'],
							'error'  => sql_errno(),
							'text'   => sql_error()
						]
					];
					break;
				} else {
					$nb_insertions += count($_enregistrements);
				}
			}
		} else {
			// Si on ne tient pas compte des erreurs c'est qu'on veut maximiser le nombre de données enregistrées. De fait,
			// - on insère enregistrement par enregistrement
			// - on ne monte aucune erreur, juste des avertissement en log afin que la transaction soit toujours committée.
			foreach ($enregistrements as $_enregistrement) {
				if (sql_insertq($table, $_enregistrement) === false) {
					// Erreur d'insertion en base de données de la cible : on loge l'erreur en avertissement
					$log = [
						'level'      => _LOG_AVERTISSEMENT,
						'code'       => 'erreur_target_storage_sql',
						'parameters' => [
							'target' => $table,
							'feed'   => $feed['feed_id'],
							'error'  => sql_errno(),
							'text'   => sql_error()
						]
					];
					$message = ezmashup_log_creer_message($log);
					spip_log($message, 'ezmashup' . $log['level']);
				} else {
					$nb_insertions += 1;
				}
			}
		}

		// On clot la transaction : rollback si erreur ou commit sinon ok
		if ($erreur) {
			// On rollback la transaction
			sql_query('ROLLBACK');
		} elseif (sql_preferer_transaction()) {
			sql_terminer_transaction();
		} else {
			sql_query('COMMIT');
		}
	} elseif ($feed['target_format'] === 'file') {
		// Seul le format JSON est accepté
		if ($feed['target_options']['extension'] === 'json') {
			// On construit le chemin du fichier cible avec l'api de ezcache pour le type target
			include_spip('inc/ezcache_cache');
			$cache = [
				'sous_dossier' => $plugin . '/' . ezmashup_target_initialiser_dossier($plugin),
				'id_target'    => $feed['target_id']
			];

			if (!cache_ecrire('ezmashup', 'target', $cache, $enregistrements)) {
				// Erreur de création du fichier de la cible
				$erreur = [
					'level'      => _LOG_ERREUR,
					'code'       => 'erreur_target_storage_fichier',
					'parameters' => [
						'target' => $feed['target_id'],
						'feed'   => $feed['feed_id']
					]
				];
			} else {
				$nb_insertions = count($enregistrements);
			}
		} else {
			// Erreur extension de fichier non supportée
			$erreur = [
				'level'      => _LOG_ERREUR,
				'code'       => 'erreur_target_storage_extension',
				'parameters' => [
					'extension' => $feed['target_options']['extension'],
					'target'    => $feed['target_id'],
					'feed'      => $feed['feed_id']
				]
			];
		}
	} else {
		// Erreur format de cible invalide
		$erreur = [
			'level'      => _LOG_ERREUR,
			'code'       => 'erreur_target_storage_format',
			'parameters' => [
				'format' => $feed['target_format'],
				'target' => $feed['target_id'],
				'feed'   => $feed['feed_id']
			]
		];
	}

	// Si une erreur est détectée, on la loge et on met le retour à false
	if ($erreur) {
		$message = ezmashup_log_creer_message($erreur);
		spip_log($message, 'ezmashup' . $erreur['level']);
	}

	return $nb_insertions;
}

/**
 * Consigne le peuplement d'un feed dans la meta propre au plugin utilisateur.
 *
 * @param string $plugin          Préfixe du plugin utilisateur.
 * @param array  $enregistrements Liste des enregistrements devant être insérés dans la table concernée (tous ne le seront pas nécessairement).
 * @param int    $nb_insertions   Nombre réels d'enregistrements stockés dans la target
 * @param array  $feed            Description complète du feed.
 *
 * @return void
 */
function dataset_target_consigner(string $plugin, array $enregistrements, int $nb_insertions, array $feed) : void {
	// Initialiser la consigne avec les informations d'exécution du feed
	$consigne['records'] = [
		'hash'   => sha1(json_encode($enregistrements)),
		'count'  => count($enregistrements),
		'insert' => $nb_insertions,
		'update' => date('Y-m-d H:i:s'),
	];

	// Compléter la consigne avec de la configuration :
	// -- des données de configuration de la cible
	$consigne['target']['format'] = $feed['target_format'];
	$consigne['target']['id'] = $feed['target_id'];
	// -- des données de configuration du feed
	$consigne['feed']['hash'] = $feed['hash'];
	$consigne['feed']['category'] = $feed['category'];
	$consigne['feed']['tags'] = $feed['tags'];
	// -- les datasets source pour connaitre les versions au moment de la création de la cible
	$consigne['sources']['basic'] = $feed['sources_basic'];
	$consigne['sources']['addon'] = $feed['sources_addon'];
	$consigne['sources']['hash'] = $feed['sources_hash'];

	// Il est possible que la consigne nécessite des compléments spécifiques via un service du plugin utilisateur.
	$consigne = ezmashup_target_completer_consigne($plugin, $consigne, $enregistrements, $feed);

	include_spip('inc/config');
	ecrire_config("{$plugin}_ezmashup/{$feed['feed_id']}", $consigne);
}

/**
 * Lit la consignation du peuplement d'un feed ou de tous les feeds d'un plugin utilisateur à partir de sa meta.
 *
 * @param string      $plugin  Préfixe du plugin utilisateur.
 * @param null|string $id_feed Identifiant du feed.
 *
 * @return array Tableau de la ou des consignations.
 */
function dataset_target_informer(string $plugin, ?string $id_feed = '') : array {
	include_spip('inc/config');
	$meta = $id_feed
		? "{$plugin}_ezmashup/{$id_feed}"
		: "{$plugin}_ezmashup";

	return lire_config($meta, []);
}

/**
 * Supprime la consignation du peuplement d'un feed.
 *
 * @param string $plugin  Préfixe du plugin utilisateur.
 * @param string $id_feed Identifiant du feed.
 *
 * @return void
 */
function dataset_target_deconsigner(string $plugin, string $id_feed) : void {
	include_spip('inc/config');
	effacer_config("{$plugin}_ezmashup/{$id_feed}");
}

/**
 * Renvoie le nom normalisé de la table SQL ou du fichier cible matérialisant le dataset cible.
 *
 * @internal
 *
 * @param string $id Nom de la table SQL ou du fichier à normaliser.
 *
 * @return array Nom de la table SQL ou du fichier normalisé.
 */
function dataset_target_normaliser_id(string $id, string $format) : string {
	// Initialiser l'id normalisée à la valeur d'erreur
	$id_normalise = '';

	if ($format === 'sql_table') {
		// On constitue la liste des tables à vérifier, les tables spip_xxx sont toujours vérifiées en premier car plus probables
		$tables_a_verifier = [$id];
		if (strpos($id, 'spip_') === false) {
			array_unshift($tables_a_verifier, "spip_{$id}");
		}

		// On verifie le ou les identifiants
		foreach ($tables_a_verifier as $_table) {
			if (sql_showtable($_table)) {
				$id_normalise = $_table;
				break;
			}
		}
	} else {
		// Forcément un fichier
		$id_normalise = identifiant_slug($id);
	}

	return $id_normalise;
}
