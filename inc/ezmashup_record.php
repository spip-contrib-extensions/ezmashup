<?php
/**
 * Ce fichier contient les fonctions d'API des `enregistrements cible` du plugin Mashup Factory.
 * *
 * * @package SPIP\EZMASHUP\RECORD\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_EZMASHUP_TARGET_SQL_NUMERIC_FIELD_TYPES')) {
	define(
		'_EZMASHUP_TARGET_SQL_NUMERIC_FIELD_TYPES',
		['BIT', 'TINYINT','BOOL', 'BOOLEAN', 'SMALLINT', 'MEDIUMINT', 'INT', 'INTEGER', 'BIGINT', 'FLOAT', 'DOUBLE', 'DECIMAL', 'DEC']
	);
}
if (!defined('_EZMASHUP_TARGET_SQL_DATE_FIELD_TYPES')) {
	define(
		'_EZMASHUP_TARGET_SQL_DATE_FIELD_TYPES',
		['DATE']
	);
}
if (!defined('_EZMASHUP_TARGET_SQL_DATETIME_FIELD_TYPES')) {
	define(
		'_EZMASHUP_TARGET_SQL_DATETIME_FIELD_TYPES',
		['DATETIME', 'TIMESTAMP']
	);
}
if (!defined('_EZMASHUP_TARGET_SQL_TIME_FIELD_TYPES')) {
	define(
		'_EZMASHUP_TARGET_SQL_TIME_FIELD_TYPES',
		['TIME']
	);
}
if (!defined('_EZMASHUP_TARGET_SQL_YEAR_FIELD_TYPES')) {
	define(
		'_EZMASHUP_TARGET_SQL_YEAR_FIELD_TYPES',
		['YEAR']
	);
}

/**
 * Remplit un enregistrement avec l'ensemble des types de champs configurés dans le feed.
 *
 * @api
 *
 * @param array      $item_source Description d'un item de la source dont certains champs seront insérés dans l'enregistrement cible
 * @param array      $feed        Description complète du feed.
 * @param null|array $erreur      Tableau constitutif d'une erreur ou vide si aucune erreur à remonter
 *
 * @return array Enregistrement de la cible mis à jour ou vide si erreur.
 */
function record_remplir(array $item_source, array $feed, ?array &$erreur = []) : array {
	// Initialisation de l'erreur éventuelle
	$erreur = [];

	// Initialisation d'un enregistrement par défaut en statique et d'un enregistrement rempli
	static $enregistrement_defaut = [];
	$enregistrement = [];

	if (!$enregistrement_defaut) {
		// Initialisation d'un élément de la table par défaut (uniquement les champs de base).
		$enregistrement_defaut = record_initialiser($feed, $erreur);
	}

	if ($enregistrement_defaut) {
		// On initialise l'enregistrement avec les valeurs par défaut
		// Cela permet de s'assurer que chaque élément du tableau de sortie aura la même structure
		// quelque soit les données lues dans la source.
		$enregistrement = $enregistrement_defaut;

		// On complète l'initialisation avec la mise à jour des champs basiques et extra provenant de l'item source.
		include_spip('inc/filtres');
		foreach ($enregistrement as $_champ => $_valeur) {
			if (array_key_exists($_champ, $feed['mapping']['basic_fields'])) {
				// C'est un champ basic, il doit y avoir un champ source pour le remplir
				$valeur_source = table_valeur($item_source, $feed['mapping']['basic_fields'][$_champ], '');
				if ($valeur_source) {
					$enregistrement[$_champ] = is_string($valeur_source)
						? trim($valeur_source)
						: $valeur_source;
				}
			} elseif (
				!empty($feed['mapping']['extra_fields'])
				and in_array($_champ, $feed['mapping']['extra_fields'])
				and isset($item_source[$_champ])
			) {
				// C'est un champ extra, ils peuvent être remplis par une fonction spécifique de la source : dans ce cas, le nom
				// du champ dans la source coincide avec celui dans la cible
				$enregistrement[$_champ] = is_string($item_source[$_champ])
					? trim($item_source[$_champ])
					: $item_source[$_champ];
			}
		}
	}

	return $enregistrement;
}
/**
 * Initialise le record d'un dataset cible avec des valeurs par défaut et remplit les champs statiques avec leur valeur finale.
 *
 * @param array      $feed   Description complète du feed.
 * @param null|array $erreur Tableau constitutif d'une erreur ou vide si aucune erreur à remonter
 *
 * @return array Enregistrement par défaut de la cible initialisé ou vide si erreur.
 */
function record_initialiser(array $feed, ?array &$erreur = []) : array {
	// Initialisation de l'erreur éventuelle
	$erreur = [];

	// Initialisation de l'enregistrement par défaut
	$enregistrement = [];

	// Lister les champs de la cible qui sont remplis par une source y compris les addons.
	$extra = $feed['mapping']['extra_fields'] ?? [];
	$static = isset($feed['mapping']['static_fields']) ? array_keys($feed['mapping']['static_fields']) : [];
	$champs = array_merge(
		array_keys($feed['mapping']['basic_fields']),
		$extra,
		$static,
	);
	if (!empty($feed['mapping']['addon_fields'])) {
		foreach ($feed['mapping']['addon_fields'] as $_addon) {
			$champs = array_merge($champs, array_keys($_addon));
		}
	}
	$champs = array_unique($champs);

	// Acquisition de la description d'un enregistrement suivant que la cible est une table SQL ou un fichier
	// de la regexp permettant d'isoler la valeur par défaut si elle existe.
	$description = $feed['target_options']['fields'];

	if (!empty($description)) {
		$unused = $feed['mapping']['unused_fields'] ?? [];
		foreach ($champs as $_champ) {
			if (isset($description[$_champ])) {
				// Le champ du mapping est bien stocké dans la cible, on peut donc l'initialiser avec sa valeur par
				// défaut configurée
				$enregistrement[$_champ] = $description[$_champ]['default'];
			} elseif (in_array($_champ, $unused)) {
				// Le champ appartient à la source mais n'est pas stocké dans la cible.
				// On l'initialise avec une chaine vide
				$enregistrement[$_champ] = '';
			} else {
				// On a un problème de configuration: on le trace et on arrête la boucle et les traitements suivants.
				$erreur = [
					'level'      => _LOG_ERREUR,
					'code'       => 'erreur_target_record_nofield',
					'parameters' => [
						'field'  => $_champ,
						'target' => $feed['target_id'],
						'feed'   => $feed['feed_id'],
					]
				];
				break;
			}
		}
	}

	// On complète l'initialisation en mettant à jour les champs statiques comme indiqué dans la configuration du feed
	if (
		!$erreur
		and $enregistrement
		and isset($feed['mapping']['static_fields'])
	) {
		include_spip('inc/filtres');
		foreach ($feed['mapping']['static_fields'] as $_champ => $_valeur) {
			if (substr($_valeur, 0, 1) === '#') {
				// -- le nom d'un tag
				$index = ltrim($_valeur, '#');
				$enregistrement[$_champ] = ($index === 'category')
					? $feed[$index]
					: $feed['tags'][$index];
			} elseif (substr($_valeur, 0, 1) === '/') {
				// -- la valeur de l'index de configuration du feed identifié par la chaine après le /
				$index = ltrim($_valeur, '/');
				$enregistrement[$_champ] = table_valeur($feed, $index, '');
			} else {
				// -- la valeur elle-même
				$enregistrement[$_champ] = $_valeur;
			}
		}
	}

	if ($erreur) {
		// Si une erreur est détectée, on la loge et on s'assure que l'enregistrement à retourner soit un tableau vide
		$enregistrement = [];
		include_spip('ezmashup/ezmashup');
		$message = ezmashup_log_creer_message($erreur);
		spip_log($message, 'ezmashup' . $erreur['level']);
	}

	return $enregistrement;
}


/**
 * Renvoie la description normalisée SQL de la table.
 *
 * @param string $table Nom de la table SQL sans le préfixe `spip_`.
 *
 * @return array Description de la table SQL normalisée.
 */
function record_normaliser_champs_sql(string $table) : array {
	// Initialiser la description normalisée
	$description = [];

	// Acquisition de la description d'un enregistrement à partir de la configuration SQL de la table
	if ($ressource = sql_query("SHOW COLUMNS FROM spip_{$table}")) {
		$description_table = sql_fetch_all($ressource);
		foreach ($description_table as $_description_colonne) {
			if (preg_match('#^([a-zA-Z]+)[\s(]*#i', trim($_description_colonne['Type']), $match)) {
				// Extraire le défaut
				$defaut = $_description_colonne['Default'];
				// Extraire le type SQL et finaliser le défaut
				$type_sql = strtoupper($match[1]);
				if (in_array($type_sql, _EZMASHUP_TARGET_SQL_NUMERIC_FIELD_TYPES)) {
					$type = 'numeric';
					$type_defaut = 0;
				} elseif (in_array($type_sql, _EZMASHUP_TARGET_SQL_DATE_FIELD_TYPES)) {
					$type = 'date';
					$type_defaut = '0000-00-00';
				} elseif (in_array($type_sql, _EZMASHUP_TARGET_SQL_DATETIME_FIELD_TYPES)) {
					$type = 'datetime';
					$type_defaut = '0000-00-00 00:00:00';
				} elseif (in_array($type_sql, _EZMASHUP_TARGET_SQL_TIME_FIELD_TYPES)) {
					$type = 'time';
					$type_defaut = '00:00:00';
				} elseif (in_array($type_sql, _EZMASHUP_TARGET_SQL_YEAR_FIELD_TYPES)) {
					$type = 'year';
					$type_defaut = 0000;
				} else {
					$type = 'string';
					$type_defaut = '';
				}
				// Ranger les informations de la colonne à son index
				$description[$_description_colonne['Field']] = [
					'type'    => $type,
					'default' => $defaut ?? $type_defaut
				];
			} else {
				$description = [];
				break;
			}
		}
	}

	return $description;
}
