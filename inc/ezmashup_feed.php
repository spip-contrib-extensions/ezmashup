<?php
/**
 * Ce fichier contient les fonctions d'API des `feeds` du plugin Mashup Factory.
 * L'API permet de charger les feeds en base de données, de les lire unitairement ou globalement et d'en manipuler
 * les datasets cible.
 *
 * @package SPIP\EZMASHUP\FEED\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Exécute, pour un feed donné d'un plugin utilisateur, le processus de mashup pour créer le dataset cible à partir
 * du ou des datasets source.
 *
 * @api
 *
 * @uses feed_lire()
 * @uses dataset_target_peupler()
 * @uses dataset_target_supprimer()
 * @uses dataset_target_stocker()
 * @uses dataset_target_consigner()
 * @uses ezmashup_feed_completer_peuplement()
 * @uses ezmashup_log_creer_message()
 *
 * @param string $plugin  Préfixe du plugin utilisateur.
 * @param string $id_feed Identifiant du feed.
 *
 * @return array Tableau résultat de l'action : soit une erreur, soit une information sur le déroulement correct.
 *               Les index sont :
 *               - int    level      : la gravité telle que définie pour les logs de spip
 *               - string code       : le code de l'information ou de l'erreur qui représente aussi l'item de langue
 *               - array  parameters : tableau associatif des paramètres de l'erreur à fournir à l'item de langue
 *
 * @throws Exception
 */
function feed_peupler(string $plugin, string $id_feed) : array {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Initialisation de l'erreur éventuelle et d'un indicateur permettant de savoir si l'erreur doit être logée
	// ou si elle a déjà été logée dans une fonction appelée
	$retour = [];
	$erreur = [];
	$loger_erreur = false;

	// Inclure les services de Mashup Factory
	include_spip('ezmashup/ezmashup');

	if ($plugin) {
		// Lire la description du feed
		$feed = feed_lire($plugin, $id_feed);
		if ($feed) {
			// Extraire les informations des sources pour produire la liste des enregistrements de la cible
			include_spip('inc/ezmashup_dataset_target');
			spip_timer('mapping');
			$enregistrements = dataset_target_peupler($plugin, $feed, $erreur);
			$duree_mapping = spip_timer('mapping');
			if (!$erreur) {
				if ($enregistrements) {
					spip_timer('storage');
					// Suppression des enregistrements existant du feed.
					$retour_vider = feed_vider($plugin, $id_feed);
					if ($retour_vider['level'] === _LOG_INFO) {
						// Insertion dans la base de données des enregistrements collectés
						$nb_enregistres = dataset_target_stocker($plugin, $enregistrements, $feed, $erreur);
						if ($nb_enregistres) {
							// On stocke les informations d'exécution dans une meta.
							dataset_target_consigner($plugin, $enregistrements, $nb_enregistres, $feed);

							// Compléter le peuplement en supprimant les caches des feeds pour s'assurer que les bons
							// hash seront renvoyés sur une requête de la collection feeds (plugin Territoires, par ex)
							// -- ce traitement peut être complété par le plugin utilisateur
							ezmashup_feed_completer_peuplement($plugin, $feed);

							// Définir le retour de la fonction
							// -- si le nombre de records stockés ne correspond pas au nombre à stocker c'est qu'il y a eu
							//    des erreurs acceptables (ex duplicate key) : on affiche quand même un avertissement
							$nb_a_enregistrer = count($enregistrements);
							$duree_storage = spip_timer('storage');
							if ($nb_enregistres === $nb_a_enregistrer) {
								$niveau = _LOG_INFO;
								$code = 'notice_feed_exec_ok';
							} else {
								$niveau = _LOG_AVERTISSEMENT;
								$code = 'notice_feed_exec_ok_mais';
							}
							$retour = [
								'level'      => $niveau,
								'code'       => $code,
								'parameters' => [
									'plugin'     => $plugin,
									'feed'       => $id_feed,
									'mapping'    => $duree_mapping,
									'storage'    => $duree_storage,
									'insert_nok' => $nb_a_enregistrer - $nb_enregistres
								]
							];
						}
					} else {
						$erreur = $retour_vider;
					}
				} else {
					// Erreur aucun enregistrement extrait sans erreur des sources du feed
					$erreur = [
						'level'      => _LOG_ERREUR,
						'code'       => 'erreur_target_norecord',
						'parameters' => [
							'plugin' => $plugin,
							'feed'   => $id_feed,
						]
					];
					$loger_erreur = true;
				}
			}
		} else {
			// Erreur lecture du feed
			$erreur = [
				'level'      => _LOG_ERREUR,
				'code'       => 'erreur_feed_api_nodesc',
				'parameters' => [
					'plugin'   => $plugin,
					'feed'     => $id_feed,
					'fonction' => 'feed_peupler'
				]
			];
			$loger_erreur = true;
		}
	} else {
		// Erreur plugin non fourni
		$erreur = [
			'level'      => _LOG_ERREUR,
			'code'       => 'erreur_feed_api_noplugin',
			'parameters' => [
				'feed'     => $id_feed,
				'fonction' => 'feed_peupler'
			]
		];
		$loger_erreur = true;
	}

	// Tracer les logs ou erreurs et finaliser le retour
	if ($erreur) {
		// Si une erreur est détectée, on la loge si nouvelle et on met l'affecte au retour de la fonction.
		$retour = $erreur;
		if ($loger_erreur) {
			$message = ezmashup_log_creer_message($erreur);
			spip_log($message, 'ezmashup' . $erreur['level']);
		}
	} else {
		// On trace l'exécution via la variable de retour qui est au format adéquat
		$message = ezmashup_log_creer_message($retour);
		spip_log($message, 'ezmashup' . $retour['level']);
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $retour;
}

/**
 * Vide, pour un feed donné, les données du dataset cible.
 * Dans le cas d'une cible au format fichier, le fichier est supprimé; dans le cas d'une table SQL, celle-ci est vidée
 * des enregistrements concernant le feed.
 *
 * @api
 *
 * @uses feed_lire()
 * @uses dataset_target_supprimer()
 * @uses ezmashup_feed_completer_vidage()
 * @uses ezmashup_log_creer_message()
 *
 * @param string $plugin  Préfixe du plugin utilisateur.
 * @param string $id_feed Identifiant du feed.
 *
 * @return array Tableau résultat de l'action : soit une erreur, soit une information sur le déroulement correct.
 *               Les index sont :
 *               - int    level      : la gravité telle que définie pour les logs de spip
 *               - string code       : le code de l'information ou de l'erreur qui représente aussi l'item de langue
 *               - array  parameters : tableau associatif des paramètres de l'erreur à fournir à l'item de langue
 *
 * @throws Exception
 */
function feed_vider(string $plugin, string $id_feed) : array {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Initialisation de l'erreur éventuelle et d'un indicateur permettant de savoir si l'erreur doit être logée
	// ou si elle a déjà été logée dans une fonction appelée
	$retour = [];
	$erreur = [];
	$loger_erreur = false;

	// Inclure les services de Mashup Factory
	include_spip('ezmashup/ezmashup');

	if ($plugin) {
		// Lire la description du feed
		$feed = feed_lire($plugin, $id_feed);
		if ($feed) {
			// Suppression des enregistrements de la cible en fonction du format de celle-ci.
			// Le feed est aussi déconsigné.
			include_spip('inc/ezmashup_dataset_target');
			if (dataset_target_supprimer($plugin, $feed, $erreur)) {
				// Compléter le vidage : Mashup Factory ne fait rien
				// -- ce traitement peut être complété par le plugin utilisateur
				ezmashup_feed_completer_vidage($plugin, $feed);

				// Définir le retour ok de la fonction
				$retour = [
					'level'      => _LOG_INFO,
					'code'       => 'notice_feed_empty_ok',
					'parameters' => [
						'plugin' => $plugin,
						'feed'   => $id_feed,
					]
				];
			}
		} else {
			// Erreur lecture du feed
			$erreur = [
				'level'      => _LOG_ERREUR,
				'code'       => 'erreur_feed_api_nodesc',
				'parameters' => [
					'plugin'   => $plugin,
					'feed'     => $id_feed,
					'fonction' => 'feed_vider'
				]
			];
			$loger_erreur = true;
		}
	} else {
		// Erreur plugin non fourni
		$erreur = [
			'level'      => _LOG_ERREUR,
			'code'       => 'erreur_feed_api_noplugin',
			'parameters' => [
				'feed'     => $id_feed,
				'fonction' => 'feed_vider'
			]
		];
		$loger_erreur = true;
	}

	// Tracer les logs ou erreurs et finaliser le retour
	if ($erreur) {
		// Si une erreur est détectée, on la loge si nouvelle et on met l'affecte au retour de la fonction.
		$retour = $erreur;
		if ($loger_erreur) {
			$message = ezmashup_log_creer_message($erreur);
			spip_log($message, 'ezmashup' . $erreur['level']);
		}
	} else {
		// On trace le vidage via la variable de retour qui est au format adéquat
		$message = ezmashup_log_creer_message($retour);
		spip_log($message, 'ezmashup' . $retour['level']);
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $retour;
}

/**
 * Retourne, pour un feed donné, la description complète.
 * Les champs JSON sont systématiquement décodés et champs textuels peuvent subir, sur demande, un traitement typo.
 *
 * @api
 *
 * @param string    $plugin       Préfixe du plugin utilisateur.
 * @param string    $id_feed      Identifiant du feed.
 * @param null|bool $traiter_typo Indique si les données textuelles doivent être retournées brutes ou si elles doivent être traitées
 *                                en utilisant la fonction typo. Par défaut l'indicateur vaut `false`.
 *                                Les champs encodés en JSON sont eux toujours décodés.
 *
 * @return array La description complète d'un feed donné. Les champs de type tableau sont systématiquement
 *               décodés et si demandé, les champs textuels peuvent être traités avec la fonction typo().
 *               Si le couple (plugin, identifiant du feed) est invalide, la fonction retourne un tableau vide.
 */
function feed_lire(string $plugin, string $id_feed, ?bool $traiter_typo = false) {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// On indexe le tableau des descriptions par le plugin appelant en cas d'appel sur le même hit
	// par deux plugins différents.
	static $description_feed = [];

	// Extraire et stocker la description du feed
	if (!isset($description_feed[$plugin][$traiter_typo][$id_feed])) {
		// Lecture de toute la configuration du feed (sauf le timestamp 'maj') : les données retournées sont brutes.
		$trouver_table = charger_fonction('trouver_table', 'base');
		$table = $trouver_table('spip_feeds');
		$select = array_diff(array_keys($table['field']), ['maj']);
		$where = [
			'plugin=' . sql_quote($plugin),
			'feed_id=' . sql_quote($id_feed)
		];
		$description = sql_fetsel($select, 'spip_feeds', $where);

		if ($description) {
			// Traitements des champs textuels
			if ($traiter_typo) {
				$description['title'] = typo($description['title']);
				if ($description['description']) {
					$description['description'] = typo($description['description']);
				}
			}

			// Traitements des champs tableaux sérialisés
			$description['tags'] = json_decode($description['tags'], true);
			$description['target_options'] = json_decode($description['target_options'], true);
			$description['mapping'] = json_decode($description['mapping'], true);
			$description['sources_basic'] = json_decode($description['sources_basic'], true);
			$description['sources_addon'] = json_decode($description['sources_addon'], true);
		}

		// Sauvegarde de la description de la page pour une consultation ultérieure dans le même hit.
		$description_feed[$plugin][$traiter_typo][$id_feed] = $description;
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $description_feed[$plugin][$traiter_typo][$id_feed];
}

/**
 * Renvoie, pour un plugin utilisateur donné ou pour tous les plugins, une liste de description brute de feeds
 * éventuellement filtrée sur certains champs.
 * Les champs JSON ne sont pas décodés.
 *
 * @api
 *
 * @param null|string $plugin        Préfixe du plugin utilisateur ou vide si tous les plugins.
 * @param null|array  $filtres       Tableau associatif `[champ] = valeur` de critères de filtres sur les descriptions de feed.
 *                                   Les opérateurs possibles sont l'égalité et la non égalité.
 * @param null|array  $champs_exclus Liste des champs du feed à exclure de la description renvoyée.
 *
 * @return array Tableau des descriptions des feeds indexé par l'id du feed ou tableau vide sinon.
 */
function feed_repertorier(?string $plugin = '', ?array $filtres = [], ?array $champs_exclus = []) : array {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// On initialise le where avec le critère sur le plugin.
	$where = [];
	if ($plugin) {
		$where = [
			'plugin=' . sql_quote($plugin)
		];
	}

	// On rajoute les filtres éventuels.
	if ($filtres) {
		foreach ($filtres as $_champ => $_critere) {
			$operateur = '=';
			$valeur = $_critere;
			if (substr($_critere, 0, 1) === '!') {
				$operateur = '!=';
				$valeur = ltrim($_critere, '!');
			}
			$where[] = $_champ . $operateur . sql_quote($valeur);
		}
	}

	// On récupère tous les champs des feeds.
	// -- Tous les champs sauf la date de mise à jour et la liste des éventuels champs exclus.
	$from = 'spip_feeds';
	$description_table = sql_showtable($from, true);
	$champs = array_keys($description_table['field']);
	$select = array_diff($champs, array_merge(['maj'], $champs_exclus));
	if ($feeds = sql_allfetsel($select, $from, $where)) {
		$feeds = array_column($feeds, null, 'feed_id');
	} else {
		$feeds = [];
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $feeds;
}

/**
 * Charge ou recharge en base de données les configurations des feeds à partir des fichiers YAML.
 * La fonction optimise le chargement en effectuant uniquement les traitements nécessaires
 * en fonction des modifications, ajouts et suppressions de feeds identifiés en comparant les md5 des fichiers YAML.
 *
 * @api
 *
 * @uses ezmashup_feed_initialiser_dossier()
 * @uses ezmashup_feed_rechercher_yaml()
 * @uses feed_categorie_repertorier()
 * @uses ezmashup_feed_categorie_initialiser_defaut()
 * @uses ezmashup_find_all_in_path()
 * @uses feed_repertorier()
 * @uses ezmashup_log_creer_message()
 *
 * @param string    $plugin    Préfixe du plugin utilisateur.
 * @param null|bool $recharger Si `true` force le rechargement de tous les feeds, sinon le chargement se base sur le
 *                             md5 des fichiers YAML. Par défaut vaut `false`.
 *
 * @return array Tableau résultat de l'action : soit une erreur, soit une information sur le déroulement correct.
 *               Les index sont :
 *               - int    level      : la gravité telle que définie pour les logs de spip
 *               - string code       : le code de l'information ou de l'erreur qui représente aussi l'item de langue
 *               - array  parameters : tableau associatif des paramètres de l'erreur à fournir à l'item de langue
 */
function feed_charger(string $plugin, ?bool $recharger = false) : array {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Initialisation de l'erreur éventuelle et d'un indicateur permettant de savoir si l'erreur doit être logée
	// ou si elle a déjà été logée dans une fonction appelée
	$retour = [];
	$erreur = [];
	$loger_erreur = false;

	// Inclure les services de Mashup Factory
	include_spip('ezmashup/ezmashup');
	include_spip('ezmashup_fonctions');

	if ($plugin) {
		if ($fichiers = ezmashup_feed_rechercher_yaml($plugin)) {
			// Initialisation des tableaux de types de noisette.
			$feeds_a_ajouter = $feeds_a_changer = [];

			// Récupération de la description complète des feeds déjà enregistrés de façon :
			// - à gérer l'activité des feeds en fin de chargement
			// - de comparer les signatures md5 des feeds déjà enregistrés. Si on force le rechargement on gère aussi
			//   les signatures pour identifier les feeds obsolètes car il faudra les décharger complètement avec leur target
			$feeds_existants = feed_repertorier($plugin);
			$signatures = array_column($feeds_existants, 'hash', 'feed_id');
			// On initialise la liste des feeds à supprimer avec l'ensemble des feeds déjà stockés.
			$feeds_a_effacer = $signatures ? array_keys($signatures) : [];

			// Lister les catégories gérées par le plugin et la catégorie par défaut :
			// -- les feeds n'appartenant pas à ces catégories sont exclus. Néanmoins, les feeds sans catégorie
			//    sont toujours inclus dans la catégorie par défaut
			$categories = array_keys(feed_categorie_repertorier($plugin));

			// Etat donné que le nom du fichier est toujours le même, le tableau des fichiers retourné ne possède qu'un
			// index 'config.yaml' pointant sur la liste des fichiers.
			// -> on aplatit le tableau facilement
			include_spip('inc/yaml');
			$dossier_base_feed = ezmashup_feed_initialiser_dossier($plugin);
			foreach ($fichiers as $_chemin) {
				// L'id du feed est le nom du répertoire conteneur des données du feed
				$id_feed = basename(dirname($_chemin));
				// Dossier du feed
				$dossier_feed = "{$dossier_base_feed}{$id_feed}/";
				// Si on a forcé le rechargement ou si aucun md5 n'est encore stocké pour le feed
				// on positionne la valeur du md5 stocké à chaine vide.
				// De cette façon, on force la lecture du fichier YAML du feed.
				$md5_stocke = (isset($signatures[$id_feed]) and !$recharger)
					? $signatures[$id_feed]
					: '';

				// On vérifie que le md5 du fichier YAML est bien différent de celui stocké avant de charger
				// le contenu. Sinon, on passe au fichier suivant.
				$md5 = md5_file($_chemin);
				if (
					($md5 !== $md5_stocke)
					and ($description = yaml_decode_file($_chemin))
					and in_array($description['category'], $categories)
				) {
					// Initialisation de la description par défaut du type de noisette
					// -- on y inclut le plugin appelant et la signature
					$description_defaut = [
						'plugin'         => $plugin,
						'feed_id'        => $id_feed,
						'title'          => $id_feed,
						'description'    => '',
						'target_format'  => 'sql_table',
						'target_id'      => '',
						'target_options' => [],
						'mapping'        => [],
						'include'        => '',
						'sources_basic'  => [],
						'sources_addon'  => [],
						'sources_hash'   => '',
						'category'       => ezmashup_feed_categorie_initialiser_defaut($plugin),
						'tags'           => [],
						'is_active'      => 'oui',
						'is_editable'    => 'non',
						'hash'           => '',
					];

					// Mise à jour du md5
					$description['hash'] = $md5;

					// Traitement de l'indicateur d'édition
					$description['is_editable'] = !empty($description['is_editable']) ? 'oui' : 'non';

					// Traitements des champs de la cible (target)
					// - on vérifie la complétude, la cohérence et on aplatit les champs
					if (
						isset($description['target'])
						and ($target = $description['target'])
						and !empty($target['id'])
					) {
						// On aplatit les options, elles seront complétées ci-après
						if (!empty($target['options'])) {
							$description['target_options'] = $target['options'];
						}
						// On aplatit l'id et le format et on les vérifie
						$description['target_format'] = $target['format'];
						if ($target['format'] === 'sql_table') {
							// Dataset cible table SQL
							// -- on normalise le nom de la table et on vérifie son existence
							include_spip('inc/ezmashup_dataset_target');
							if ($table = dataset_target_normaliser_id($target['id'], $target['format'])) {
								$description['target_id'] = $table;
								// On complète les options avec la description normalisée de la table
								if (
									include_spip('inc/ezmashup_record')
									and ($description_table = record_normaliser_champs_sql($target['id']))
								) {
									$description['target_options']['fields'] = $description_table;
									// et la clé primaire de façon à rester cohérent avec la cible au format fichier
									$description['target_options']['primary_key'] = [];
									include_spip('base/objets');
									if ($id_table = id_table_objet($description['target_id'])) {
										// On stocke la clé sous forme de liste pour les tests d'appartenance.
										$description['target_options']['primary_key'] = explode(',', $id_table);
									}
								} else {
									$description['is_active'] = 'tgt';
								}
							} else {
								$description['is_active'] = 'tgt';
							}
						} elseif ($target['format'] === 'file') {
							// Dataset cible format fichier
							// -- On normalise le nom du fichier en appliquant une slugification
							$description['target_id'] = dataset_target_normaliser_id($target['id'], $target['format']);
							$description['target_options']['extension'] = 'json';
							if ($target['format'] !== 'file') {
								$description['is_active'] = 'tgt';
							} elseif (!isset($description['target_options']['fields'])) {
								$description['is_active'] = 'tgt';
							} elseif (!isset($description['target_options']['primary_key'])) {
								$description['is_active'] = 'tgt';
							}
						} else {
							$description['is_active'] = 'tgt';
						}
					} else {
						$description['is_active'] = 'tgt';
					}
					unset($description['target']);

					// Complétude de la description avec les valeurs par défaut
					$description = array_merge($description_defaut, $description);

					// Normaliser le titre et la description pour stocker les traductions en multi et pas les idiomes
					$description['title'] = ezmashup_normaliser_idiome($plugin, $description['title']);
					$description['description'] = ezmashup_normaliser_idiome($plugin, $description['description']);

					// Normalisation des tags
					if (null === $description['tags']) {
						$description['tags'] = [];
					}

					// Normalisation du champ représentant l'include éventuel contenant des fonctions spécifiques
					if ($description['include']) {
						// Normalisation de l'include
						$description['include'] = ezmashup_normaliser_include($description['include'], $dossier_feed);
						// Vérification de l'existence de l'include
						if (!include_spip($description['include'], false)) {
							$description['is_active'] = 'inc';
						}
					} else {
						// On cherche si l'include standard du plugin utilisateur existe. Si non, c'est qu'aucun
						// include n'est nécessaire : ce n'est pas une erreur
						$include = "ezmashup/{$plugin}";
						if (include_spip($include, false)) {
							$description['include'] = $include;
						}
					}

					// Vérification du mapping
					if (!isset($description['mapping']['basic_fields'])) {
						$description['is_active'] = 'map';
					} elseif (
						isset($description['mapping']['depth_fields'])
						and ($depth = $description['mapping']['depth_fields'])
						and !isset($depth['name'], $depth['base_value'], $depth['parent_field'], $depth['parent_value'])
					) {
						$description['is_active'] = 'dep';
					} elseif (
						isset($description['mapping']['has_label_field'])
						and !in_array($description['mapping']['has_label_field'], ['pre', 'post', 'non'], true)
					) {
						$description['is_active'] = 'lbl';
					}
					// On normalise l'indicateur de label multilangue si il n'est pas fourni
					if (!isset($description['mapping']['has_label_field'])) {
						$description['mapping']['has_label_field'] = 'non';
					}

					// Vérification et normalisation des options de la cible
					$description['target_options'] = array_merge(
						[
							'extension'     => '',
							'max_chunk'     => 0,
							'stop_on_error' => true,
							'feed_column'   => ''
						],
						$description['target_options']
					);
					if (
						isset($description['mapping']['static_fields'])
						and ($colonne = array_search('/feed_id', $description['mapping']['static_fields']))
					) {
						$description['target_options']['feed_column'] = $colonne;
					}

					// Vérification et normalisation des datasets primaires
					// - il faut au moins un dataset
					// - on utilise le dataset '*' comme un objet par défaut si il existe
					include_spip('inc/ezmashup_dataset_source');
					if (empty($description['sources_basic'])) {
						$description['is_active'] = 'bas';
					} else {
						// Vérifier si un dataset '*' existe et l'utiliser comme dataset par défaut après l'avoir normalisé
						$dataset_etoile = $description['sources_basic']['*'] ?? [];
						unset($description['sources_basic']['*']);
						// Normaliser les datasets
						if ($description['sources_basic']) {
							$options = [
								'include_feed' => $description['include'],
								'dossier_feed' => $dossier_feed
							];
							$dataset_etoile = dataset_source_normaliser_configuration($dataset_etoile, $options);
							$options['dataset_defaut'] = $dataset_etoile;
							foreach ($description['sources_basic'] as $_dataset_id => $_dataset) {
								$description['sources_basic'][$_dataset_id] = dataset_source_normaliser_configuration($_dataset, $options);
							}
						} else {
							$description['is_active'] = 'bas';
						}
					}

					// Vérification et normalisation des datasets secondaires
					// - on compare la liste des datasets utilisés dans le mapping et la liste de ceux identfiés
					$mapping_datasets = array_keys($description['mapping']['addon_fields'] ?? []);
					$addons_datasets = $description['sources_addon'] ?? [];
					if (array_diff($mapping_datasets, array_keys($addons_datasets))) {
						$description['is_active'] = 'add';
					}
					// - on normalise les datasets secondaires
					foreach ($description['sources_addon'] as $_dataset_id => $_dataset) {
						$description['sources_addon'][$_dataset_id] = dataset_source_normaliser_configuration($_dataset);
					}

					// Fin des vérifications, on loge une erreur de configuration sous la forme d'un warning spip
					if ($description['is_active'] !== 'oui') {
						// On trace l'exclusion sans générer d'erreur
						$log = [
							'level'      => _LOG_AVERTISSEMENT,
							'code'       => 'erreur_feed_config_' . $description['is_active'],
							'parameters' => [
								'feed' => $id_feed,
							]
						];
						$message = ezmashup_log_creer_message($log);
						spip_log($message, 'ezmashup' . $log['level']);
					}

					// Déterminer et stocker le md5 des datasets source
					$description['sources_hash'] = md5(json_encode(array_merge($description['sources_basic'], $description['sources_addon'])));

					// Encodage JSON des champs de type tableau
					$description['tags'] = json_encode($description['tags']);
					$description['target_options'] = json_encode($description['target_options']);
					$description['mapping'] = json_encode($description['mapping']);
					$description['sources_basic'] = json_encode($description['sources_basic']);
					$description['sources_addon'] = json_encode($description['sources_addon']);

					if (
						!$md5_stocke
						or $recharger
					) {
						// Le feed est soit nouveau soit on est en mode rechargement forcé:
						// => il faut le rajouter.
						$feeds_a_ajouter[] = $description;
						// => et il faut donc le supprimer de la liste des feeds obsolètes
						$feeds_a_effacer = array_diff($feeds_a_effacer, [$id_feed]);
					} else {
						// La description stockée a été modifiée et le mode ne force pas le rechargement:
						// => il faut mettre à jour le feed.
						$feeds_a_changer[] = $description;
						// => et il faut donc le supprimer de la liste des feeds obsolètes
						$feeds_a_effacer = array_diff($feeds_a_effacer, [$id_feed]);
					}
				} elseif ($md5 === $md5_stocke) {
					// Le feed n'a pas changé et n'a donc pas été rechargé:
					// => Il faut donc juste indiquer qu'il n'est pas obsolète.
					$feeds_a_effacer = array_diff($feeds_a_effacer, [$id_feed]);

				}	// Sinon c'est que le feed est ou est devenu illisible ou de catégorie invalide pour le plugin :
					// => il faut le décharger
			}

			// Mise à jour des feeds en base de données :
			// -- Suppression des feeds obsolètes ou de tous les feeds si on est en mode rechargement forcé.
			// -- Update des feeds modifiés.
			// -- Insertion des nouveaux feeds.
			$from = 'spip_feeds';
			$where = [
				'plugin=' . sql_quote($plugin)
			];
			if (sql_preferer_transaction()) {
				sql_demarrer_transaction();
			}
			// -- Déchargement (données + feeds) des feeds à effacer car il se sont jamais rechargés
			if ($feeds_a_effacer) {
				foreach ($feeds_a_effacer as $_id_feed) {
					feed_decharger($plugin, $_id_feed);
				}
			}
			// -- Si on recharge les feeds, suppression des feeds restant du plugin (les feeds mais pas leurs données)
			if ($recharger) {
				sql_delete($from, $where);
			}
			// -- Update des feeds modifiés
			if ($feeds_a_changer) {
				sql_replace_multi($from, $feeds_a_changer);
			}
			// -- Insertion des nouveaux feeds
			if ($feeds_a_ajouter) {
				sql_insertq_multi($from, $feeds_a_ajouter);
			}
			if (sql_preferer_transaction()) {
				sql_terminer_transaction();
			}

			// Définir le retour ok de la fonction
			$retour = [
				'level'      => _LOG_INFO,
				'code'       => 'notice_feed_load_ok',
				'parameters' => [
					'plugin' => $plugin,
				]
			];
		}
	} else {
		// Erreur plugin non fourni
		$erreur = [
			'level'      => _LOG_ERREUR,
			'code'       => 'erreur_feed_api_noplugin',
			'parameters' => [
				'fonction' => 'feed_charger'
			]
		];
		$loger_erreur = true;
	}

	// Tracer les logs ou erreurs et finaliser le retour
	if ($erreur) {
		// Si une erreur est détectée, on la loge si nouvelle et on met l'affecte au retour de la fonction.
		$retour = $erreur;
		if ($loger_erreur) {
			$message = ezmashup_log_creer_message($erreur);
			spip_log($message, 'ezmashup' . $erreur['level']);
		}
	} else {
		// On trace le chargement via la variable de retour qui est au format adéquat
		$message = ezmashup_log_creer_message($retour);
		spip_log($message, 'ezmashup' . $retour['level']);
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $retour;
}

/**
 * Vide, pour un plugin utilisateur donné, le feed concerné ou à défaut tous les feeds de la table `spip_feeds`,
 * les données des datasets cible associés et les consignations.
 *
 * @api
 *
 * @uses feed_repertorier()
 * @uses feed_vider()
 * @uses ezmashup_log_creer_message()
 *
 * @param string      $plugin  Préfixe du plugin utilisateur.
 * @param null|string $id_feed Identifiant du feed.
 *
 * @return array Tableau résultat de l'action : soit une erreur, soit une information sur le déroulement correct.
 *               Les index sont :
 *               - int    level      : la gravité telle que définie pour les logs de spip
 *               - string code       : le code de l'information ou de l'erreur qui représente aussi l'item de langue
 *               - array  parameters : tableau associatif des paramètres de l'erreur à fournir à l'item de langue
 *
 * @throws Exception
 */
function feed_decharger(string $plugin, ?string $id_feed = '') : array {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Initialisation de l'erreur éventuelle et d'un indicateur permettant de savoir si l'erreur doit être logée
	// ou si elle a déjà été logée dans une fonction appelée
	$retour = [];
	$erreur = [];
	$loger_erreur = false;

	// Inclure les services de Mashup Factory
	include_spip('ezmashup/ezmashup');

	if ($plugin) {
		// Répertorier tous les feeds du plugin ou uniquement le feed fourni en argument
		$filtres = [];
		if ($id_feed) {
			$filtres['feed_id'] = $id_feed;
		}
		$feeds = feed_repertorier($plugin, $filtres);
		if ($feeds) {
			// Supprimer les datasets cible de chaque feed
			foreach ($feeds as $_feed) {
				// Si le feed est peuplé, suppression des enregistrements de la cible en fonction du format de celle-ci
				// et déconsignation.
				// -- on utilise la fonction de vidage pour être sur que tous les traitements de vidage sont réalisés.
				$retour_vider = feed_vider($plugin, $_feed['feed_id']);
				if ($retour_vider['level'] !== _LOG_INFO) {
					$erreur = $retour_vider;
					break;
				}
			}

			if (!$erreur) {
				// Supprimer la meta de consignation des feeds du plugin si elle est devenue vide
				include_spip('inc/config');
				if (!lire_config("{$plugin}_ezmashup", [])) {
					effacer_config("{$plugin}_ezmashup");
				}

				// Supprimer le feed concerné ou tous les feeds de la table spip_feeds
				$where = [
					'plugin=' . sql_quote($plugin)
				];
				if ($id_feed) {
					$where[] = 'feed_id=' . sql_quote($id_feed);
				}
				sql_delete('spip_feeds', $where);

				// Définir le retour ok de la fonction
				$retour = [
					'level'      => _LOG_INFO,
					'code'       => $id_feed ? 'notice_feed_unload_1_ok' : 'notice_feed_unload_ok',
					'parameters' => [
						'plugin'  => $plugin,
						'feed_id' => $id_feed
					]
				];
			}
		}
	} else {
		// Erreur plugin non fourni
		$erreur = [
			'level'      => _LOG_ERREUR,
			'code'       => 'erreur_feed_api_noplugin',
			'parameters' => [
				'fonction' => 'feed_decharger'
			]
		];
		$loger_erreur = true;
	}

	// Tracer les logs ou erreurs et finaliser le retour
	if ($erreur) {
		// Si une erreur est détectée, on la loge si nouvelle et on l'affecte au retour de la fonction.
		$retour = $erreur;
		if ($loger_erreur) {
			$message = ezmashup_log_creer_message($erreur);
			spip_log($message, 'ezmashup' . $erreur['level']);
		}
	} else {
		// On trace le déchargement via la variable de retour qui est au format adéquat
		$message = ezmashup_log_creer_message($retour);
		spip_log($message, 'ezmashup' . $retour['level']);
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $retour;
}

/**
 * Supprime un feed éditable.
 * Cela consiste à supprimer le dossier du feed et l'ensemble de ses ressources (config.yaml et les fichiers sources éventuels).
 *
 * Les données du feed sont au préalable vidées et le feed lui-même est déchargé de la base.
 *
 * @api
 *
 * @param string $plugin  Préfixe du plugin utilisateur.
 * @param string $id_feed Identifiant du type de noisette.
 *
 * @return array Tableau résultat de l'action : soit une erreur, soit une information sur le déroulement correct.
 *               Les index sont :
 *               - int    level      : la gravité telle que définie pour les logs de spip
 *               - string code       : le code de l'information ou de l'erreur qui représente aussi l'item de langue
 *               - array  parameters : tableau associatif des paramètres de l'erreur à fournir à l'item de langue
 *
 * @throws Exception
 */
function feed_supprimer(string $plugin, string $id_feed) : array {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Initialisation de l'erreur éventuelle et d'un indicateur permettant de savoir si l'erreur doit être logée
	// ou si elle a déjà été logée dans une fonction appelée
	$retour = [];
	$erreur = [];
	$loger_erreur = false;

	if ($plugin) {
		// Inclure l'api de Cache Factory et des services de Mashup Factory
		include_spip('inc/ezcache_cache');
		include_spip('ezmashup/ezmashup');

		// On supprime le dossier du feed et ses ressources :
		//  -- recherche du fichier config.yaml pour déterminer le dossier du feed, sans gérer d'erreur car le feed existe forcément
		$cache = [
			'sous_dossier' => "{$plugin}/" . ezmashup_feed_initialiser_dossier($plugin) . "{$id_feed}/",
		];
		if ($fichier_config = cache_est_valide('ezmashup', 'config', $cache)) {
			// On supprime le dossier complet
			include_spip('inc/flock');
			$chemin_feed = dirname($fichier_config);
			if (supprimer_repertoire($chemin_feed)) {
				// Décharger le feed (vidage des données et suppression de la table en base de données)
				$retour_decharger = feed_decharger($plugin, $id_feed);
				if ($retour_decharger['level'] === _LOG_INFO) {
					// Définir le retour ok de la fonction
					$retour = [
						'level'      => _LOG_INFO,
						'code'       => 'notice_feed_delete_ok',
						'parameters' => [
							'plugin'  => $plugin,
							'feed_id' => $id_feed
						]
					];
				} else {
					// On renvoie l'erreur mais on ne la loge pas car cela est déjà fait par la fonction feed_decharger()
					$erreur = $retour_decharger;
				}
			} else {
				// Erreur suppression des ressources
				$erreur = [
					'level'      => _LOG_ERREUR,
					'code'       => 'erreur_feed_delete_resource',
					'parameters' => [
						'plugin'  => $plugin,
						'feed_id' => $id_feed
					]
				];
				$loger_erreur = true;
			}
		}
	} else {
		// Erreur plugin non fourni
		$erreur = [
			'level'      => _LOG_ERREUR,
			'code'       => 'erreur_feed_api_noplugin',
			'parameters' => [
				'fonction' => 'feed_supprimer'
			]
		];
		$loger_erreur = true;
	}

	// Tracer les logs ou erreurs et finaliser le retour
	if ($erreur) {
		// Si une erreur est détectée, on la loge si nouvelle et on l'affecte au retour de la fonction.
		$retour = $erreur;
		if ($loger_erreur) {
			$message = ezmashup_log_creer_message($erreur);
			spip_log($message, 'ezmashup' . $erreur['level']);
		}
	} else {
		// On trace la suppression via la variable de retour qui est au format adéquat
		$message = ezmashup_log_creer_message($retour);
		spip_log($message, 'ezmashup' . $retour['level']);
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $retour;
}

/**
 * Vérifie si un fichier ressource existe dans le dossier d'accueil du feed éditable concerné.
 * Le fichier peut-être le `config.yaml` du feed ou une source de données.
 *
 * @api
 *
 * @param string $plugin    Préfixe du plugin utilisateur.
 * @param string $id_feed   Identifiant du type de noisette.
 * @param array  $ressource Identifie la ressource :
 *                          - index `type` : `config` pour le fichier `config.yaml`, `source` pour un fichier source au format YAML, JSON ou CSV
 *                          - index `format` : définit le format de la ressource, `csv`, `json`, `yaml` ou `xml`
 *                          - index `id` : chaine représentant l'id de la source
 *
 * @return string Le chemin complet du fichier si valide, la chaine vide sinon.
 */
function feed_ressource_existe(string $plugin, string $id_feed, array $ressource) : string {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Inclure l'api de Cache Factory et des services de Mashup Factory
	include_spip('inc/ezcache_cache');
	include_spip('ezmashup/ezmashup');

	// Le type de ressource coincide avec le type de cache
	$type_ressource = $ressource['type'] ?? 'config';

	// Le sous-dossier est toujours le même quelque soit le type de ressource.
	$cache = [
		'sous_dossier' => "{$plugin}/" . ezmashup_feed_initialiser_dossier($plugin) . "{$id_feed}/",
	];
	// Les sources de données possède un nom en plus du préfixe
	$type_cache = $type_ressource;
	if ($type_ressource !== 'config') {
		$type_cache = "{$type_cache}_{$ressource['format']}";
		$cache['id_source'] = $ressource['id'] ?? '';
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return cache_est_valide('ezmashup', $type_cache, $cache);
}

/**
 * Lit et décode un fichier ressource dans le dossier d'accueil des feeds éditables du plugin utilisateur.
 * Le fichier peut-être le `config.yaml` du feed ou une source de données.
 *
 * @param string $plugin    Préfixe du plugin utilisateur.
 * @param string $id_feed   Identifiant du type de noisette.
 * @param array  $ressource Identifie la ressource :
 *                          - index `type` : `config` pour le fichier config.yaml, `source` pour un fichier
 *                          source de format YAML, JSON ou CSV
 *                          - index `format` : définit le format de la ressource, `csv`, `json`, `yaml` ou `xml`
 *                          - index `decodage` : pour un fichier csv permet de préciser le délimiteur
 *                          - index `id` : chaine représentant l'id de la source
 *
 * @return array|bool|string Contenu du fichier sous la forme d'un tableau, d'une chaine ou false si une erreur s'est
 *                           produite.
 *
 * @throws Exception
 */
function feed_ressource_lire(string $plugin, string $id_feed, array $ressource) {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Inclure l'api de Cache Factory et des services de Mashup Factory
	include_spip('inc/ezcache_cache');
	include_spip('ezmashup/ezmashup');

	// Le type de ressource permet de définir le type de cache
	$type_ressource = $ressource['type'] ?? 'config';

	// Le sous-dossier est toujours le même quelque soit le type de ressource.
	$cache = [
		'sous_dossier' => "{$plugin}/" . ezmashup_feed_initialiser_dossier($plugin) . "{$id_feed}/",
	];
	// Les sources de données possède un nom en plus du préfixe et parfois un codage en csv qui précise le délimiteur
	$type_cache = $type_ressource;
	if ($type_ressource !== 'config') {
		$type_cache = "{$type_cache}_{$ressource['format']}";
		$cache['id_source'] = $ressource['id'] ?? '';

		// Si le format est CSV, il faut préciser le codage exact à partir du délimiteur
		if ($ressource['format'] === 'csv') {
			$codage = array_search($ressource['decodage'], configuration_codage_csv_lire());
			$cache['codage'] = $codage !== false ? $codage : '';
		}
	}

	// Lecture du fichier si celui-ci est valide
	$contenu = false;
	if ($fichier_cache = cache_est_valide('ezmashup', $type_cache, $cache)) {
		// Lecture des données du fichier cache valide et peuplement de l'index données
		$contenu = cache_lire('ezmashup', $type_cache, $fichier_cache);
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $contenu;
}

/**
 * Encode un tableau et écrit le résultat dans un fichier ressource du d'accueil des feeds éditable du plugin utilisateur.
 * Le fichier peut-être le `config.yaml` du feed ou une source de données.
 *
 * @api
 *
 * @param string       $plugin    Préfixe du plugin utilisateur.
 * @param string       $id_feed   Identifiant du type de noisette.
 * @param array        $ressource Identifie la ressource :
 *                                - index `type` : `config` pour le fichier config.yaml, `source` pour un fichier
 *                                source de format YAML, JSON ou CSV
 *                                - index `format` : définit le format de la ressource, `csv`, `json`, `yaml` ou `xml`
 *                                - index `decodage` : pour un fichier csv permet de préciser le délimiteur
 *                                - index `id` : chaine représentant l'id de la source
 * @param array|string $contenu   Données à inscrire dans le fichier déjà encodé ou non. C'est Cache
 *                                Factory qui s'en occupera.
 *
 * @return string Le chemin complet si la ressource a bien été écrite, chaine vide sinon.
 */
function feed_ressource_ecrire(string $plugin, string $id_feed, array $ressource, $contenu) : string {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Inclure l'api de Cache Factory et des services de Mashup Factory
	include_spip('inc/ezcache_cache');
	include_spip('ezmashup/ezmashup');

	// Le type de ressource permet de définir le type de cache
	$type_ressource = $ressource['type'] ?? 'config';

	// Le sous-dossier est toujours le même quelque soit le type de ressource.
	$cache = [
		'sous_dossier' => "{$plugin}/" . ezmashup_feed_initialiser_dossier($plugin) . "{$id_feed}/",
	];
	// Les sources de données possède un nom en plus du préfixe et parfois un codage en csv qui précise le délimiteur
	$type_cache = $type_ressource;
	if ($type_ressource !== 'config') {
		$type_cache = "{$type_cache}_{$ressource['format']}";
		$cache['id_source'] = $ressource['id'] ?? '';

		// Si le format est CSV, il faut préciser le codage exact à partir du délimiteur
		if ($ressource['format'] === 'csv') {
			$codage = array_search($ressource['decodage'], configuration_codage_csv_lire());
			$cache['codage'] = $codage !== false ? $codage : '';
		}
	}

	// Ecriture du fichier
	// -- en cas d'erreur on renvoie un chemin vide
	$fichier_cache = '';
	if (cache_ecrire('ezmashup', $type_cache, $cache, $contenu)) {
		// On récupère le chemin complet du fichier cache à écrire
		$fichier_cache = cache_nommer('ezmashup', $type_cache, $cache);
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $fichier_cache;
}
