<?php
/**
 * Gestion du formulaire d'administration des feeds d'un plugin utilisateur.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement du formulaire d'administration des feeds.
 * Il est obligatoire de fournir un préfixe de plugin utilisateur sinon le formulaire n'est pas utilisable.
 *
 * @param string      $plugin          Préfixe du plugin utilisateur
 * @param null|string $id_categorie    Identifiant de la catégorie ou vide si on veut afficher tous les feeds
 * @param null|string $id_feed_affiche Identificant du feed servant à l'affichage du bloc de détails
 * @param null|string $module          Identificant du module dans lequel piocher les items de langue (`ezmashup` par défaut)
 * @param null|string $page_admin      URL de la page d'admin dans laquelle le présent formulaire est inclus
 *
 * @return array Environnement du formulaire
 **/
function formulaires_admin_feeds_charger_dist(string $plugin, ?string $id_categorie = '', ?string $id_feed_affiche = '', ?string $module = '', ?string $page_admin = '') {
	$valeurs = [];

	if (autoriser('voir', '_feeds', '', null, ['plugin' => $plugin])) {
		$valeurs['plugin'] = $plugin;
		if ($id_categorie) {
			$valeurs['category'] = $id_categorie;
		}
		$valeurs['feed_id'] = $id_feed_affiche;
		// Items de langue personnalisables et module de langue à utiliser
		$valeurs['module'] = $module ?: 'ezmashup';
		$valeurs['info_0_feed'] = texte_script(_T("{$valeurs['module']}:info_0_feed"));
		$valeurs['info_1_feed'] = texte_script(_T("{$valeurs['module']}:info_1_feed"));
		$valeurs['info_nb_feed'] = texte_script(_T("{$valeurs['module']}:info_nb_feed"));
	} else {
		$valeurs['message_erreur'] = _T('ezmashup:erreur_feed_admin_noaccess');
		$valeurs['editable'] = false;
	}

	return $valeurs;
}

/**
 * Traitement du formulaire d'administration des feeds.
 * Soit l'action demandée est lancée immédiatement (`peupler`, `vider`, `supprimer`), soit l'action (`creer`) renvoie
 * vers une page dans laquelle elle sera réellement traitée.
 *
 * @param string      $plugin          Préfixe du plugin utilisateur
 * @param null|string $id_categorie    Identifiant de la catégorie ou vide si on veut afficher tous les feeds
 * @param null|string $id_feed_affiche Identificant du feed servant à l'affichage du bloc de détails
 * @param null|string $module          Identificant du module dans lequel piocher les items de langue (`ezmashup` par défaut)
 * @param null|string $page_admin      URL de la page d'admin dans laquelle le présent formulaire est inclus
 *
 * @return array Retour du traitement : rien de spécial car pour l'instant, il n'est pas utilisé
 **/
function formulaires_admin_feeds_traiter_dist(string $plugin, ?string $id_categorie = '', ?string $id_feed_affiche = '', ?string $module = '', ?string $page_admin = '') {
	$retour = [];

	// Identifier la liste des actions activables par le formulaire
	include_spip('ezmashup_fonctions');
	$filtres = [
		'menu' => '!'
	];
	$actions_admin = feed_action_repertorier($plugin, $filtres);

	// Identification de l'action choisie et de l'id du feed concerné
	$id_action = '';
	$id_feed = '';
	foreach ($actions_admin as $_id => $_action) {
		if ($saisie = _request($_id)) {
			$id_action = $_id;
			$id_feed = key($saisie);
			break;
		}
	}

	// Traitement de l'action demandée
	if ($id_feed) {
		// L'action demandée :
		// - soit déclenche un traitement sur le feed
		// - soit déclenche l'affichage du formulaire
		if ($actions_admin[$id_action]['type'] === 'action') {
			// On déclenche l'action demandée
			$actionner = charger_fonction(
				"{$id_action}_feed",
				'action',
				true
			);
			$retour_action = $actionner("{$plugin}:{$id_feed}");
			$parametres = $retour_action['parameters'] ?? [];
			if ($retour_action['level'] === _LOG_ERREUR) {
				// On affiche un message d'erreur simple suivi par l'erreur exacte remontée du traitement. Ce complément
				// est affiché en italique
				$message_erreur = ($id_action === 'peupler')
					? _T('ezmashup:notice_feed_exec_nok', $parametres)
					: _T('ezmashup:notice_feed_empty_nok', $parametres);
				$retour['message_erreur'] = $message_erreur
					. '<br />'
					. '<i>(' . _T('ezmashup:' . $retour_action['code'], $parametres) . ')</i>';
			} else {
				// On affiche le message correspondant à l'information ou la notice remontée du traitement
				$retour['message_ok'] = _T('ezmashup:' . $retour_action['code'], $parametres);
			}
		} else {
			// On renvoie vers la page de traitement de l'action dont l'url est à personnaliser par le plugin utilisateur :
			$retour['redirect'] = feed_action_definir_url($plugin, $id_action, $id_feed, $page_admin);
		}
	}

	$retour['editable'] = true;

	return $retour;
}
