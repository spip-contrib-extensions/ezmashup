<?php
/**
 * Ce fichier contient la personnalisation des services du plugin Cache Factory.
 * Mashup Factory utilise ce plugin pour gérer toutes les ressources de type fichier qu'il crée lui-même : dataset cible,
 * config.yaml et dataset source d'un feed éditable.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie la configuration spécifique des caches gérés par Mashup Factory pour les datasets cible, le fichier
 * config.yaml et les datasets source.
 *
 * @param string $plugin Préfixe du plugin utilisateur..
 *
 * @return array Tableau de la configuration brute du plugin Taxonomie.
 */
function ezmashup_cache_configurer(string $plugin) : array {
	// Un type de cache par type de ressource
	// -- dataset cible de type fichier
	$configuration = [
		'target' => [
			'racine'          => '_DIR_ETC',
			'sous_dossier'    => true,
			'nom_prefixe'     => 'target',
			'nom_obligatoire' => ['id_target'],
			'nom_facultatif'  => [],
			'extension'       => '.json',
			'securisation'    => false,
			'serialisation'   => false,
			'codage'          => 'json',
			'separateur'      => '-',
			'conservation'    => 0,
			'administration'  => false
		],
		// -- fichier de configuration de feed, config.yaml
		'config' => [
			'racine'          => '_DIR_ETC',
			'sous_dossier'    => true,
			'nom_prefixe'     => 'config',
			'nom_obligatoire' => [],
			'nom_facultatif'  => [],
			'extension'       => '.yaml',
			'securisation'    => false,
			'serialisation'   => false,
			'codage'          => 'yaml',
			'separateur'      => '',
			'conservation'    => 0,
			'administration'  => false
		],
		// -- dataset source au format JSON
		'source_json' => [
			'racine'          => '_DIR_ETC',
			'sous_dossier'    => true,
			'nom_prefixe'     => 'source',
			'nom_obligatoire' => ['id_source'],
			'nom_facultatif'  => [],
			'extension'       => '.json',
			'securisation'    => false,
			'serialisation'   => false,
			'codage'          => 'json',
			'separateur'      => '-',
			'conservation'    => 0,
			'administration'  => false
		],
	];

	// On recopie la config de la source json pour le xml et le csv car seule l'extension change
	// -- dataset source au format XML
	$configuration['source_xml'] = $configuration['source_json'];
	$configuration['source_xml']['extension'] = '.xml';
	$configuration['source_xml']['codage'] = 'xml';
	// -- dataset source au format CSV
	$configuration['source_csv'] = $configuration['source_json'];
	$configuration['source_csv']['extension'] = '.csv';
	$configuration['source_csv']['codage'] = 'csv';

	return $configuration;
}
