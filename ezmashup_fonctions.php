<?php
/**
 * Ce fichier contient les fonction d'API concernant les catégories et les actions ainsi que les balises du plugin.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Compile la balise `#FEED_CATEGORIES` qui fournit la configuration d'une ou des catégories affectées aux différents
 * feeds, pour un plugin utilisateur donné.
 * La signature de la balise est : `#FEED_CATEGORIES{plugin[, id_categorie, information]}`.
 *
 * @balise
 *
 * @example
 *     ```
 *     #FEED_CATEGORIES{isocode}, renvoie la description complète des catégories des feeds du plugin `isocode`.
 *     #FEED_CATEGORIES{isocode, map}, renvoie la description de la catégorie `map` des feeds du plugin `isocode`.
 *     #FEED_CATEGORIES{isocode, map, name}, renvoie le nom de la catégorie `map` des feeds du plugin `isocode`.
 *     ```
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_FEED_CATEGORIES_dist(Champ $p) : Champ {
	// Récupération des arguments de la balise.
	// -- Préfixe du plugin (obligatoire)
	$plugin = interprete_argument_balise(1, $p);
	$plugin = isset($plugin) ? str_replace('\'', '"', $plugin) : '""';
	// -- Identifiant de la catégorie (facultatif)
	$id_categorie = interprete_argument_balise(2, $p);
	$id_categorie = isset($id_categorie) ? str_replace('\'', '"', $id_categorie) : '""';
	// -- Identifiant de la catégorie (facultatif)
	$information = interprete_argument_balise(3, $p);
	$information = isset($information) ? str_replace('\'', '"', $information) : '""';

	// Calcul de la balise
	$p->code = "feed_categorie_repertorier({$plugin}, {$id_categorie}, {$information})";

	return $p;
}

/**
 * Renvoie tout ou partie de la liste des catégories de feed et de leur description.
 * Il est possible de demander toutes les catégories ou juste une seule désignée par son identifiant voire une seule
 * information sur une catégorie donnée.
 *
 * @api
 *
 * @uses ezmashup_feed_categorie_lister()
 *
 * @param string      $plugin       Préfixe du plugin utilisateur.
 * @param null|string $id_categorie Identifiant de la catégorie ou chaine vide pour toutes les catégories.
 * @param null|string $information  Information de la description d'une catégorie (name, description, icon).
 *
 * @return array|string Liste des descriptions de catégorie, une description d'une seule catégorie voire une information.
 */
function feed_categorie_repertorier(string $plugin, ?string $id_categorie = '', ?string $information = '') {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Stocker la liste descriptions de catégorie indexées par l'identifiant pour éviter le recalcul sur le même hit.
	static $categories = [];

	// Si vide, chargement de la liste des catégories: celles par défaut de Mashup Factory et les compléments par pipeline.
	if (!isset($categories[$plugin])) {
		include_spip('ezmashup/ezmashup');
		$categories[$plugin] = ezmashup_feed_categorie_lister($plugin);
	}

	// Extraction de toutes les catégories d'un plugin, d'une seule ou d'une information d'une catégorie
	if (
		$id_categorie
		and isset($categories[$plugin][$id_categorie])
	) {
		if (
			$information
			and isset($categories[$plugin][$id_categorie][$information])
		) {
			$retour = $categories[$plugin][$id_categorie][$information];
		} else {
			$retour = $categories[$plugin][$id_categorie];
		}
	} else {
		$retour = $categories[$plugin];
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $retour;
}

/**
 * Construit la liste des types de source du feed pour un affichage.
 *
 * @param array       $sources    Tableau des sources du feed
 * @param null|string $separateur Séparateur des tags dans la liste affichée (par défaut une virgule)
 *
 * @return string Liste des types de source au format chaine
 */
function feed_source_lister(array $sources, ?string $separateur = ',') : string {
	// On renvoie les types de sources et leur nombre
	$affichage = '';

	// Extraction des types et calcul de leur nombre
	$types = [];
	foreach ($sources as $_source) {
		$types[$_source['source']['type']] = isset($types[$_source['source']['type']])
			? $types[$_source['source']['type']] + 1
			: 1;
	}

	// Construction de la liste à afficher
	$separateur = trim($separateur) . ' ';
	foreach ($types as $_type => $_nb) {
		$affichage .= ($affichage && $_nb ? $separateur : '') . _T('ezmashup:type_source_' . $_type) . ' (' . $_nb . ')';
	}

	return $affichage;
}

/**
 * Construit la liste des tags non techniques du feed pour un affichage.
 * Les tags techniques commencent par un `_`.
 *
 * @param array       $tags       Tableau des tags du feed
 * @param null|string $separateur Séparateur des tags dans la liste affichée (par défaut une virgule)
 *
 * @return string Liste des tags non techniques au format chaine
 */
function feed_tag_lister(array $tags, ?string $separateur = ',') : string {
	// On exclut de l'affichage les tags techniques commençant par un '_'
	$affichage = '';
	$separateur = trim($separateur) . ' ';
	foreach ($tags as $_tag => $_valeur) {
		if (substr($_tag, 0, 1) !== '_') {
			$affichage .= ($affichage && $_valeur ? $separateur : '') . $_valeur;
		}
	}

	return $affichage;
}

/**
 * Compile la balise `#FEED_URL_ACTION` qui fournit l'url de la page de création ou d'édition d'un feed éditable.
 * La signature de la balise est : `#FEED_URL_ACTION{plugin, action[, id_feed, page_admin]}`.
 *
 * @balise
 *
 * @example
 *     ```
 *     #FEED_URL_ACTION{isocode, creer}.
 *     #FEED_URL_ACTION{isocode, editer, id_du_feed, page_admin}.
 *     ```
 *
 * @param Champ $p Pile au niveau de la balise
 *
 * @return Champ Pile complétée par le code à générer.
 */
function balise_FEED_URL_ACTION_dist(Champ $p) : Champ {
	// Récupération des arguments de la balise.
	// -- Préfixe du plugin (obligatoire)
	$plugin = interprete_argument_balise(1, $p);
	$plugin = isset($plugin) ? str_replace('\'', '"', $plugin) : '""';
	// -- Identifiant de l'action d'administration du feed
	$action = interprete_argument_balise(2, $p);
	$action = isset($action) ? str_replace('\'', '"', $action) : '""';
	// -- Identifiant du feed
	$id_feed = interprete_argument_balise(3, $p);
	$id_feed = isset($id_feed) ? str_replace('\'', '"', $id_feed) : '""';
	// -- URL de la page d'admin qui a initiée l'action
	$page_admin = interprete_argument_balise(4, $p);
	$page_admin = isset($page_admin) ? str_replace('\'', '"', $page_admin) : '""';

	// Calcul de la balise
	$p->code = "feed_action_definir_url({$plugin}, {$action}, {$id_feed}, {$page_admin})";

	return $p;
}

/**
 * Fournit l'url de la page de création ou d'édition d'un feed éditable.
 *
 * @api
 *
 * @uses feed_lire()
 * @uses ezmashup_feed_action_definir_url()
 * @uses ezmashup_log_creer_message()
 *
 * @param string      $plugin     Préfixe du plugin utilisateur.
 * @param string      $action     Action de création (`creer`) ou d'édition (`editer`) du feed.
 * @param null|string $id_feed    Identifiant du feed, uniquement pour l'édition.
 * @param null|string $page_admin URL de la page d'admin d'où provient l'action.
 *
 * @return string URL de la page de traitement correspondant à l'action ou vide si erreur.
 *                Les erreurs sont tracées mais pas retournées.
 */
function feed_action_definir_url(string $plugin, string $action, ?string $id_feed = '', ?string $page_admin = '') : string {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Initialisation de l'url à vide qui représente une erreur
	$url = '';
	$trace = [];
	$erreur = [];
	$loger_erreur = false;

	// Inclure les services de Mashup Factory
	include_spip('ezmashup/ezmashup');

	if ($plugin) {
		// Acquérir la liste des actions nécessitant une redirection
		$filtres = [
			'type' => 'redirect'
		];
		$actions = feed_action_repertorier($plugin, $filtres);

		// Vérifier la cohérence des données fournies
		$feed = [];
		if (
			(in_array($action, array_keys($actions)))
			and (
				!$actions[$action]['feed']
				or (
					$actions[$action]['feed']
					and include_spip('inc/ezmashup_feed')
					and ($feed = feed_lire($plugin, $id_feed))
				)
			)
		) {
			// Si l'action est créer ou éditer on acquiert la page contenant le formulaire idoine
			if ($url = ezmashup_feed_action_definir_url($plugin, $action, $feed, $page_admin)) {
				// Définir le retour ok de la fonction
				$trace = [
					'level'      => _LOG_INFO,
					'code'       => 'notice_feed_admin_ok',
					'parameters' => [
						'plugin' => $plugin,
						'feed'   => $id_feed,
						'action' => $action,
						'admin'  => $page_admin
					]
				];
			} else {
				// Erreur lecture du feed
				$erreur = [
					'level'      => _LOG_ERREUR,
					'code'       => 'erreur_feed_api_nourl',
					'parameters' => [
						'plugin'   => $plugin,
						'feed'     => $id_feed,
						'action'   => $action,
						'fonction' => 'feed_action_definir_url'
					]
				];
				$loger_erreur = true;
			}
		} else {
			// Erreur lecture du feed
			$erreur = [
				'level'      => _LOG_ERREUR,
				'code'       => 'erreur_feed_api_nodesc',
				'parameters' => [
					'plugin'   => $plugin,
					'feed'     => $id_feed,
					'fonction' => 'feed_action_definir_url'
				]
			];
			$loger_erreur = true;
		}
	} else {
		// Erreur plugin non fourni
		$erreur = [
			'level'      => _LOG_ERREUR,
			'code'       => 'erreur_feed_api_noplugin',
			'parameters' => [
				'feed'     => $id_feed,
				'fonction' => 'feed_action_definir_url'
			]
		];
		$loger_erreur = true;
	}

	// Tracer les logs ou erreurs et finaliser le retour
	if ($erreur) {
		// Si une erreur est détectée, on la loge si nouvelle et on met l'affecte au retour de la fonction.
		if ($loger_erreur) {
			$message = ezmashup_log_creer_message($erreur);
			spip_log($message, 'ezmashup' . $erreur['level']);
		}
	} else {
		// On trace le calcul de l'URL via la variable de retour qui est au format adéquat
		$message = ezmashup_log_creer_message($trace);
		spip_log($message, 'ezmashup' . $trace['level']);
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $url;
}

/**
 * Compile la balise `#FEED_ACTIONS` qui fournit, pour un plugin utilisateur, la configuration de liste des actions possibles sur les feeds
 * éventuellement filtrée sur un ou plusieurs critères.
 * La signature de la balise est : `#FEED_ACTIONS{plugin[, filtres]}`.
 *
 * @balise
 *
 * @example
 *     ```
 *     #FEED_ACTIONS{isocode}, renvoie la description complète des actions possibles.
 *     ```
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_FEED_ACTIONS_dist(Champ $p) : Champ {
	// Récupération des arguments de la balise.
	// -- Préfixe du plugin (obligatoire)
	$plugin = interprete_argument_balise(1, $p);
	$plugin = isset($plugin) ? str_replace('\'', '"', $plugin) : '""';
	// -- Identifiant de la catégorie (facultatif)
	$filtres = interprete_argument_balise(2, $p);
	$filtres = isset($filtres) ? str_replace('\'', '"', $filtres) : '[]';

	// Calcul de la balise
	$p->code = "feed_action_repertorier({$plugin}, {$filtres})";

	return $p;
}

/**
 * Renvoie tout ou partie de la liste des actions sur les feeds et de leur configuration.
 * Il est possible de filtrer les actions suivant un ou plusieurs critères.
 *
 * @api
 *
 * @uses ezmashup_feed_action_lister()
 *
 * @param string     $plugin  Préfixe du plugin utilisateur.
 * @param null|array $filtres Filtres éventuels sur un ou plusieurs champs d'une action. Seule l'égalité est gérée.
 *
 * @return array|string Liste des configurations d'actions éventuellement filtrée.
 */
function feed_action_repertorier(string $plugin, ?array $filtres = []) : array {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Stocker la liste descriptions de catégorie indexées par l'identifiant pour éviter le recalcul sur le même hit.
	static $actions = [];

	// Si vide, chargement de la liste des catégories: celles par défaut de Mashup Factory et les compléments par pipeline.
	if (!isset($actions[$plugin])) {
		// Initialisation des actions par défaut
		// -- actions lancées par des boutons isolés non liées à un feed
		$actions[$plugin] = [
			'charger' => [
				'name' => '<:ezmashup:bouton_recharger:>',
				'icon' => '',
				'type' => 'action',
				'auth' => 'charger',
				'feed' => false,
				'menu' => '',
				'bord' => false
			],
			'creer' => [
				'name' => '<:ezmashup:bouton_creer:>',
				'icon' => '',
				'type' => 'redirect',
				'auth' => 'creer',
				'feed' => false,
				'menu' => '',
				'bord' => false
			],
			'peupler' => [
				'name' => '<:ezmashup:bouton_peupler:>',
				'icon' => '',
				'type' => 'action',
				'auth' => 'executer',
				'feed' => true,
				'menu' => 'executer',
				'bord' => false
			],
			'vider' => [
				'name' => '<:ezmashup:bouton_vider:>',
				'icon' => '',
				'type' => 'action',
				'auth' => 'executer',
				'feed' => true,
				'menu' => 'executer',
				'bord' => false
			],
			'voir' => [
				'name' => '<:ezmashup:bouton_voir:>',
				'icon' => '',
				'type' => 'action',
				'auth' => 'voir',
				'feed' => false,
				'menu' => '',
				'bord' => false
			],
			'editer' => [
				'name' => '<:ezmashup:bouton_editer:>',
				'icon' => '',
				'type' => 'redirect',
				'auth' => 'modifier',
				'feed' => true,
				'menu' => 'actionner',
				'bord' => false
			],
			'supprimer' => [
				'name' => '<:ezmashup:bouton_supprimer:>',
				'icon' => '',
				'type' => 'action',
				'auth' => 'modifier',
				'feed' => true,
				'menu' => 'actionner',
				'bord' => false
			],
		];
		$id_reserves = array_keys($actions[$plugin]);

		// Ajouter les actions supplémentaires proposées par le plugin utilisateur
		include_spip('ezmashup/ezmashup');
		$actions_plugin = ezmashup_feed_action_completer_liste($plugin);
		if ($actions_plugin) {
			// On vérifie les actions
			foreach ($actions_plugin as $_id => $_action) {
				// -- ne pas modifier les actions standard
				if (in_array($_id, $id_reserves)) {
					unset($actions_plugin[$_id]);
				} else {
					// -- valeurs par défaut
					$action_defaut = [
						'name' => $_id,
						'icon' => '',
						'type' => '',
						'auth' => $_id,
						'feed' => true,
						'menu' => 'actionner',
						'bord' => false
					];
					$_action = array_merge($action_defaut, $_action);
					// -- l'action doit appartenir au groupe personnalisables : si non on le force
					if (
						!isset($_action['menu'])
						or ($_action['menu'] !== 'actionner')
					) {
						$actions_plugin[$_id]['menu'] = 'actionner';
					}
				}
			}
			// On ajoute les nouvelles actions
			$actions[$plugin] = array_merge($actions[$plugin], $actions_plugin);
		}
	}

	// Filtrage éventuel de la liste
	$retour = $actions[$plugin];
	if ($filtres) {
		foreach ($retour as $_id => $_action) {
			foreach ($filtres as $_critere => $_valeur) {
				$operateur_egalite = true;
				$valeur = $_valeur;
				if (substr($_valeur, 0, 1) == '!') {
					$operateur_egalite = false;
					$valeur = ltrim($_valeur, '!');
				}
				if (
					!isset($_action[$_critere])
					or (
						isset($_action[$_critere])
						and (($operateur_egalite and ($_action[$_critere] !== $valeur))
							or (!$operateur_egalite and ($_action[$_critere] === $valeur))))
				) {
					unset($retour[$_id]);
					break;
				}
			}
		}
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $retour;
}


/**
 * Vérifie si une liste d'actions sur les feeds possède au moins une action autorisée.
 *
 * @api
 *
 * @param string $plugin  Préfixe du plugin utilisateur.
 * @param string $id_feed Identifiant du feed, uniquement pour l'édition
 * @param array  $actions Liste des actions pour lesquelles vérifier l'autorisation.
 *
 * @return bool `true` si au moins une action est autorisée, `false` sinon
 */
function feed_action_liste_autorisee(string $plugin, string $id_feed , array $actions) : bool {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// On initialise l'autorisation à false sachant que l'on cherche la première action autorisée
	$est_autorise = false;

	// On sort à la première autorisation à vrai
	foreach ($actions as $_action) {
		if (autoriser($_action['auth'], 'feed', $id_feed, null, ['plugin' => $plugin])) {
			$est_autorise = true;
			break;
		}
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $est_autorise;
}

/**
 * Compile la balise `#EZMASHUP_PLUGINS` qui fournit les plugins utilisateur actifs implémentant des feeds.
 * La signature de la balise est : `#EZMASHUP_PLUGINS{[actifs_seuls]}`.
 *
 * @balise
 *
 * @example
 *     ```
 *     #EZMASHUP_PLUGINS, renvoie la liste des préfixes de plugins actifs utilisant Mashup Factory.
 *     #EZMASHUP_PLUGINS{oui}, renvoie la liste des préfixes de plugins actifs utilisant Mashup Factory.
 *     #EZMASHUP_PLUGINS{non}, renvoie la liste des préfixes de tous plugins utilisant Mashup Factory.
 *     ```
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_EZMASHUP_PLUGINS_dist(Champ $p) : Champ {
	// Récupération des arguments de la balise.
	// -- Indicateur plugins actifs seulement ou tous
	$actifs_seuls = interprete_argument_balise(1, $p);
	$actifs_seuls = isset($actifs_seuls) ? str_replace('\'', '"', $actifs_seuls) : '"oui"';

	// Calcul de la balise
	$p->code = "ezmashup_lister_plugins({$actifs_seuls})";

	return $p;
}

/**
 * Renvoie la liste des préfixes des plugins actifs ou de tous les plugins utilisant Mashup Factory.
 * L'utilisation du plugin Mashup Factory est repérée via les consignations dans la table `spip_meta`.
 *
 * @param string $actifs_seuls Indique de renvoyer seulement les plugins actifs (`oui`) ou tous les plugins
 *
 * @return array Liste des préfixes en minuscules
 */
function ezmashup_lister_plugins($actifs_seuls = 'oui') {
	static $plugins = [];

	// Eviter les valeurs erronées en rendant l'indicateur binarie
	$exclure_inactif = ($actifs_seuls === 'oui');

	if (!isset($plugins[$exclure_inactif])) {
		$prefixes = [];

		// On cherche les plugins utilisateurs à partir de la table spip_feeds.
		// En effet, dans la meta de consignation il n'y a que les feeds peuplés, on pourrait donc rater un plugin
		// avec que des feeds non peuplés.
		$feeds = sql_allfetsel('plugin', 'spip_feeds', [], ['plugin']);
		if ($feeds) {
			$feeds = array_column($feeds, 'plugin');
			foreach ($feeds as $_prefixe) {
				if (
					(
						$exclure_inactif
						and defined('_DIR_PLUGIN_' . strtoupper($_prefixe))
					)
					or !$exclure_inactif
				) {
					$prefixes[] = $_prefixe;
				}
			}
		}

		// Enregistrement de la liste
		$plugins[$exclure_inactif] = $prefixes;
	}

	return $plugins[$exclure_inactif];
}
