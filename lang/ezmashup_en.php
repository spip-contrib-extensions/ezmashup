<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/ezmashup-ezmashup?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_actionner' => 'Actions',
	'bouton_creer' => 'Create a feed',
	'bouton_editer' => 'Edit',
	'bouton_peupler' => 'Populate',
	'bouton_recharger' => 'Reload feeds',
	'bouton_supprimer' => 'Delete',
	'bouton_vider' => 'Empty',
	'bouton_voir' => 'Feeds',

	// D
	'description_feed_category_default' => 'Default category to which feeds with no category are assigned.',

	// E
	'erreur_feed_admin_noaccess' => 'You are not authorized to manage feeds',
	'erreur_feed_admin_plugin' => 'No user plug-in provided on the form',
	'erreur_feed_api_nodesc' => 'error reading @feed@ feed configuration in @fonction@ function',
	'erreur_feed_api_noplugin' => 'no user plug-in provided to the @fonction@ function for the @feed@ feed',
	'erreur_feed_api_nourl' => 'error calculating URL for the @action@ action on the @feed@ feed in the @function@ function',
	'erreur_feed_config_add' => 'The configuration of the `sources_addon` block is incorrect.',
	'erreur_feed_config_bas' => 'The configuration of the `sources_basic` block cannot be found',
	'erreur_feed_config_dep' => 'The configuration of the `depth_fields` block is incorrect.',
	'erreur_feed_config_inc' => 'The include configuration is incorrect',
	'erreur_feed_config_lbl' => 'Multilanguage label mode is incorrectly configured',
	'erreur_feed_config_map' => 'The configuration of the `basic_fields` block cannot be found',
	'erreur_feed_config_tgt' => 'The configuration of the `target` block is incorrect',
	'erreur_feed_delete_resource' => 'The @feed@ feed resources have not been deleted correctly. The context of the feed may have become inconsistent.',
	'erreur_source_extract' => '@format@ decoding error in @type_source@/@source@ source',
	'erreur_source_extract_callback' => '@type_source@/@source@ source decoding error by @callback@ callback',
	'erreur_source_extract_format' => 'invalid @format@ format for @type_source@/@source@ source decoding',
	'erreur_source_extract_request' => '@url@ URL source request error',
	'erreur_source_extract_type' => 'invalid @type@ type for @type_source@/@source@ source extraction',
	'erreur_source_extract_xml' => '@type_source@/@source@ XML source decoding error (@message@)',
	'erreur_source_file' => 'the @uri@ file of the @type_source@/@source@ source cannot be found',
	'erreur_target_delete_fichier' => 'error deleting @target@ target file from @feed@ feed',
	'erreur_target_delete_sql' => 'error deleting @target@ SQL target from @feed@ feed',
	'erreur_target_norecord' => 'no record extracted from the @feed@ feed sources provided by the @plugin@ plugin',
	'erreur_target_record_nofield' => 'le champ @field@ n’est pas un champ de la cible @target@ du feed @feed@',
	'erreur_target_record_nokey' => 'error populating the @feed@ feed due to a record without a primary key',
	'erreur_target_storage_extension' => '@extension@ extension not supported for file storage of @target@ target of @feed@ feed.',
	'erreur_target_storage_fichier' => 'error storing @target@ target file in @feed@ feed',
	'erreur_target_storage_format' => '@format@ storage format error for @target@ target in @feed@ feed',
	'erreur_target_storage_sql' => '@target@ SQL target storage error in @feed@ feed. Error #@error@ - @text@',

	// I
	'info_0_feed' => 'No feed',
	'info_1_feed' => '1 feed',
	'info_feed_aucun' => 'This category offers no feed',
	'info_feed_config_maj' => 'Update available',
	'info_feed_non_peuple' => 'Not yet populated',
	'info_feed_peuple' => '@nb@ records populated on @date@',
	'info_nb_feed' => '@nb@ feeds',
	'info_plugin_aucun' => 'No user plugin available',

	// L
	'label_feed_category_default' => 'Miscellaneous',
	'label_feed_details_basic_source' => 'Basic source',
	'label_feed_details_id' => 'Identifier',
	'label_feed_details_include' => 'Include',
	'label_feed_details_plugin' => 'Plugin providing feeds',
	'label_feed_details_tags' => 'Tags',
	'label_feed_details_target' => 'Target',
	'label_feed_list_category' => 'Category',
	'label_feed_list_id' => 'ID',
	'label_feed_list_record' => 'Records',
	'label_feed_list_target' => 'Target',
	'label_feed_list_title' => 'Title',
	'label_feed_list_update' => 'Date',
	'lien_feed_details_moins' => 'Read less',
	'lien_feed_details_plus' => 'Read more',

	// N
	'notice_feed_admin_ok' => 'The URL for the @action@ action in the @feed@ feed was correctly calculated',
	'notice_feed_delete_ok' => 'The @feed@ feed was successfully deleted',
	'notice_feed_empty_nok' => 'Emptying the @feed@ feed went wrong',
	'notice_feed_empty_ok' => 'Emptying of @feed@ feed completed successfully',
	'notice_feed_exec_nok' => 'Execution of @feed@ feed went wrong',
	'notice_feed_exec_ok' => 'The @feed@ feed was successfully completed',
	'notice_feed_exec_ok_mais' => 'The @feed@ feed was successfully executed, but some data (@insert_nok@) was not inserted (@insert_nok@)',
	'notice_feed_load_ok' => 'Feeds loaded correctly',
	'notice_feed_unload_1_ok' => 'The @feed@ feed was successfully unloaded',
	'notice_feed_unload_ok' => 'Feeds unloaded correctly',

	// P
	'placeholder_filtrer_feeds' => 'Filter',

	// T
	'titre_form_admin' => 'Feeds management',
	'titre_page_feeds' => 'Feeds list',
	'type_source_api' => 'API',
	'type_source_file' => 'file',
	'type_source_page' => 'webpage',
];
