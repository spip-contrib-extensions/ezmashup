<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/ezmashup.git

return [

	// B
	'bouton_actionner' => 'Actions',
	'bouton_creer' => 'Créer un feed',
	'bouton_editer' => 'Editer',
	'bouton_peupler' => 'Peupler',
	'bouton_recharger' => 'Recharger les feeds',
	'bouton_supprimer' => 'Supprimer',
	'bouton_vider' => 'Vider',
	'bouton_voir' => 'Feeds',

	// D
	'description_feed_category_default' => 'Catégorie par défaut à laquelle sont affectés les feeds sans catégorie.',

	// E
	'erreur_feed_admin_noaccess' => 'Vous n’êtes pas autorisé à administrer les feeds',
	'erreur_feed_admin_plugin' => 'Aucun plugin utilisateur fourni au formulaire',
	'erreur_feed_api_nodesc' => 'erreur de lecture de la configuration du feed @feed@ dans la fonction @fonction@',
	'erreur_feed_api_noplugin' => 'aucun plugin utilisateur fourni à la fonction @fonction@ pour le feed @feed@',
	'erreur_feed_api_nourl' => 'erreur de calcul de l’URL pour l’action @action@ du feed @feed@ dans la fonction @fonction@',
	'erreur_feed_config_add' => 'La configuration du bloc `sources_addon` est incorrecte',
	'erreur_feed_config_bas' => 'La configuration du bloc `sources_basic` est introuvable',
	'erreur_feed_config_dep' => 'La configuration du bloc `depth_fields` est incorrecte',
	'erreur_feed_config_inc' => 'La configuration de l’include est incorrecte',
	'erreur_feed_config_lbl' => 'La configuration du mode label multilangues est incorrrecte',
	'erreur_feed_config_map' => 'La configuration du bloc `basic_fields` est introuvable',
	'erreur_feed_config_tgt' => 'La configuration du bloc `target` est incorrecte',
	'erreur_feed_delete_resource' => 'Les ressources du feed @feed@ n’ont pas été correctement supprimées. Il est possible que le contexte du feed soit devenu incohérent.',
	'erreur_source_extract' => 'erreur de décodage de la source @type_source@/@source@ de format @format@)',
	'erreur_source_extract_callback' => 'erreur de décodage de la source @type_source@/@source@ par la callback @callback@',
	'erreur_source_extract_format' => 'format @format@ invalide pour le décodage de la source @type_source@/@source@',
	'erreur_source_extract_request' => 'erreur de requêtage de la source d’URL @url@',
	'erreur_source_extract_type' => 'type @type@ invalide pour l’extraction de la source @type_source@/@source@',
	'erreur_source_extract_xml' => 'erreur de décodage de la source XML @type_source@/@source@ (@message@)',
	'erreur_source_file' => 'le fichier @uri@ de la source @type_source@/@source@ est introuvable',
	'erreur_target_delete_fichier' => 'erreur de suppression de la cible fichier @target@ du feed @feed@',
	'erreur_target_delete_sql' => 'erreur de suppresion de la cible SQL @target@ du feed @feed@',
	'erreur_target_norecord' => 'aucun enregistrement extrait des sources du feed @feed@ fourni par le plugin @plugin@',
	'erreur_target_record_nofield' => 'le champ @field@ n’est pas un champ de la cible @target@ du feed @feed@',
	'erreur_target_record_nokey' => 'erreur de peuplement du feed @feed@ due à un enregistrement sans clé primaire',
	'erreur_target_storage_extension' => 'extension @extension@ non supportée pour le stockage fichier de la target @target@ du feed @feed@',
	'erreur_target_storage_fichier' => 'erreur de stockage de la cible fichier @target@ du feed @feed@',
	'erreur_target_storage_format' => 'erreur de format de stockage @format@ pour la target @target@ du feed @feed@',
	'erreur_target_storage_sql' => 'erreur de stockage de la cible SQL @target@ du feed @feed@. Erreur SQL @error@ - @text@',

	// I
	'info_0_feed' => 'Aucun feed',
	'info_1_feed' => '1 feed',
	'info_feed_aucun' => 'Aucun feed disponible',
	'info_feed_config_maj' => 'Mise à jour disponible',
	'info_feed_non_peuple' => 'Non encore peuplé',
	'info_feed_peuple' => '@nb@ enregistrements peuplés le @date@',
	'info_nb_feed' => '@nb@ feeds',
	'info_plugin_aucun' => 'Aucun plugin utilisateur disponible',

	// L
	'label_feed_category_default' => 'Divers',
	'label_feed_details_basic_source' => 'Source primaire',
	'label_feed_details_id' => 'Identifiant',
	'label_feed_details_include' => 'Include',
	'label_feed_details_plugin' => 'Plugin fournisseur',
	'label_feed_details_tags' => 'Tags',
	'label_feed_details_target' => 'Cible',
	'label_feed_list_category' => 'Catégorie',
	'label_feed_list_id' => 'Id',
	'label_feed_list_record' => 'Enregistrements',
	'label_feed_list_target' => 'Cible',
	'label_feed_list_title' => 'Titre',
	'label_feed_list_update' => 'Le',
	'lien_feed_details_moins' => 'Moins d’infos',
	'lien_feed_details_plus' => 'Plus d’infos',

	// N
	'notice_feed_admin_ok' => 'Le calcul de l’URL de l’action @action@ du feed @feed@ s’est correctement déroulée',
	'notice_feed_delete_ok' => 'La suppression complète du feed @feed@ s’est correctement déroulée',
	'notice_feed_empty_nok' => 'Le vidage du feed @feed@ s’est mal déroulé',
	'notice_feed_empty_ok' => 'Le vidage du feed @feed@ s’est correctement terminé',
	'notice_feed_exec_nok' => 'L’exécution du feed @feed@ s’est mal déroulée',
	'notice_feed_exec_ok' => 'L’exécution du feed @feed@ s’est correctement terminée',
	'notice_feed_exec_ok_mais' => 'L’exécution du feed @feed@ s’est correctement terminée mais des données n’ont pas été insérées (@insert_nok@)',
	'notice_feed_load_ok' => 'Le chargement des feeds s’est correctement déroulé',
	'notice_feed_unload_1_ok' => 'Le déchargement du feed @feed@ s’est correctement déroulé',
	'notice_feed_unload_ok' => 'Le déchargement des feeds s’est correctement déroulé',

	// P
	'placeholder_filtrer_feeds' => 'Filtrer',

	// T
	'titre_form_admin' => 'Administration des feeds',
	'titre_page_feeds' => 'Liste des feeds',
	'type_source_api' => 'API',
	'type_source_file' => 'fichier',
	'type_source_page' => 'page web',
];
