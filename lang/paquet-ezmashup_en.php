<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/ezmashup-paquet-xml-ezmashup?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// E
	'ezmashup_description' => 'Using a simple API, this plugin makes it easy to create a mashup from scattered data in file, REST API or web page.',
	'ezmashup_slogan' => 'Mashup creation made easy',
];
