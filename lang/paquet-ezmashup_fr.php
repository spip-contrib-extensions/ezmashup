<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/ezmashup.git

return [

	// E
	'ezmashup_description' => 'Ce plugin permet, au travers d’une API simple, de créer facilement un mashup à partir de données éparses au format fichier, API REST ou page web.',
	'ezmashup_slogan' => 'Faciliter la création de mashup',
];
