<?php
/**
 * Ce fichier contient la personnalisation des services du plugin REST Factory.
 * Le plugin Mashup Factory utilise ces services pour exposer via une API REST la collection des feeds.
 *
 * @package SPIP\EZMASHUP\REST
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// --------------------------------------------------------------------
// ------------------------ COLLECTION FEEDS --------------------------
// --------------------------------------------------------------------

/**
 * Récupère les configurations des feeds ainsi que leur statu de peuplement.
 * Il est possible de filtrer la liste avec les critères facultatifs `categorie` ou `plugin`.
 *
 * @param array $conditions    Conditions à appliquer au select
 * @param array $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array $configuration Configuration de la collection utile pour savoir quelle fonction appeler pour
 *                             construire chaque filtre.
 *
 * @return array Tableau des feeds.
 */
function feeds_collectionner(array $conditions, array $filtres, array $configuration) : array {
	// Initialisation de la collection
	$feeds = [];

	// Récupérer la liste des feeds (filtrée ou pas).
	// -- on utilise pas la fonction feed_repertorier car le filtre sur les catégories n'est pas pris en compte (liste
	//    d'id séparés par des virgules)
	$from = 'spip_feeds';
	$description_table = sql_showtable($from, true);
	$champs = array_keys($description_table['field']);
	// -- Exclure certains champs inutiles
	$select = array_diff($champs, ['is_active', 'target_options', 'mapping', 'include', 'maj']);

	// -- Initialisation du where: le feed doit être actif.
	$where = [
		'is_active=' . sql_quote('oui')
	];
	// -- Si il y a des critères additionnels on complète le where en conséquence.
	if ($conditions) {
		$where = array_merge($where, $conditions);
	}

	// -- Lecture de la table
	$feeds_actifs = sql_allfetsel($select, $from, $where);

	// Il faut maintenant vérifier que les feeds sont peuplés et rajouter les informations associées
	include_spip('inc/ezmashup_dataset_target');
	foreach ($feeds_actifs as $_feed) {
		// Recherche des informations de consignation de peuplement et consolidation des informations dans une liste
		// indexée par l'id de chaque feed.
		if ($consigne = dataset_target_informer($_feed['plugin'], $_feed['feed_id'])) {
			$feeds[$_feed['feed_id']] = $_feed;
			$feeds[$_feed['feed_id']]['tags'] = json_decode($feeds[$_feed['feed_id']]['tags'], true);
			$feeds[$_feed['feed_id']]['records'] = $consigne['records'];
		}
	}

	return $feeds;
}

/**
 * Calcule la condition du filtre `categorie` pour lequel il est possible de passer une liste d'identifiants séparés
 * par une virgule.
 *
 * @param string $valeur Valeur du critère `categorie`.
 *
 * @return string La condition SQL sur le champ `category` de la table `spip_feeds`.
 */
function feeds_conditionner_categorie(string $valeur) : string {
	$condition = '';
	if ($valeur) {
		if (strpos($valeur, ',') === false) {
			// Une seule catégorie
			$condition = 'category=' . sql_quote($valeur);
		} else {
			// Une liste de pays séparés par des virgules
			$categories = explode(',', $valeur);
			$condition = sql_in('category', $categories);
		}
	}

	return $condition;
}
