<?php
/**
 * Ce fichier contient les fonctions de service de Mashup Factory pour les autorisations, les feeds, les datasets et les
 * catégories.
 *
 * Chaque fonction, soit aiguille, si elle existe, vers une fonction "homonyme" propre au plugin appelant
 * ou à un feed, soit déroule sa propre implémentation.
 * Ainsi, les plugins externes peuvent, si elle leur convient, utiliser l'implémentation proposée par Mashup Factory
 * en codant un minimum de fonctions.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_EXTRAIRE_MULTI')) {
	/**
	 * Restaure cette constante à partir de SPIP 4.2.
	 */
	define('_EXTRAIRE_MULTI', '@<multi>(.*?)</multi>@sS');
}
if (!defined('_EXTRAIRE_IDIOME')) {
	/**
	 * Extrait les composants d'un idiome de langue utilisable dans un YAML
	 * '@<:(?:([a-z0-9_]+):)?([a-z0-9_]+):>@isS'.
	 */
	define('_EXTRAIRE_IDIOME', ',<:(?:([a-z0-9_]+):)?([a-z0-9_]*):/?>,iS');
}

// -----------------------------------------------------------
// -------------------- AUTORISATIONS ------------------------
// -----------------------------------------------------------

/**
 * Renvoie l'autorisation d'un plugin utilisateur de Mashup Factory, soit celle de base, soit celle d'une action donnée.
 *
 * Le plugin Mashup Factory autorise uniquement les administrateurs complets sauf pour les actions créer
 * et modifier qui sont non autorisées par défaut.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @uses ezmashup_chercher_service()
 *
 * @param string         $plugin Préfixe du plugin utilisateur.
 * @param null|array|int $qui    L'initiateur de l'action:
 *                               - si null on prend alors visiteur_session
 *                               - un id_auteur (on regarde dans la base)
 *                               - un tableau auteur complet, y compris [restreint]
 * @param null|string    $action Type d'action appliquée à un feed ou à tous les feeds
 * @param null|array     $feed   Description du feed concerné par l'action ou vide si inutile
 *
 * @return bool `true`si l'auteur est autorisée à l'utilisation de base du plugin Mashup Factory, `false` sinon.
 */
function ezmashup_plugin_autoriser(string $plugin, $qui, ?string $action = '', ?array $feed = []) : bool {
	//spip_log(' deb ' . __FUNCTION__ . ' action ' . $action, 'ezmashup_trace' . _LOG_DEBUG);
	// Si le plugin utilisateur veut définir une autorisation spécifique, il doit fournir un service associé.
	$fonction = 'plugin_autoriser' . ($action ? "_{$action}" : '');
	if ($autoriser = ezmashup_chercher_service($plugin, $fonction)) {
		$autorise = $autoriser($qui, $feed);
	} elseif (
		($action === 'creer')
		or ($action === 'modifier')
	) {
		$autorise = false;
	} else {
		include_spip('inc/autoriser');
		$autorise = autoriser('defaut');
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $autorise;
}

// -----------------------------------------------------------------------
// ------------------------------ FEEDS ----------------------------------
// -----------------------------------------------------------------------

/**
 * Renvoie la configuration par défaut du dossier relatif où trouver les feeds.
 *
 * Le service de Mashup Factory considère que par défaut le dossier relatif des feeds est `feeds/`.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @uses ezmashup_chercher_service()
 *
 * @param string $plugin Préfixe du plugin utilisateur.
 *
 * @return string Chemin relatif du dossier où chercher les feeds.
 */
function ezmashup_feed_initialiser_dossier(string $plugin) : string {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Initialisation de la catégorie par défaut en statique pour améliorer les performances
	static $dossiers = [];

	if (!isset($dossiers[$plugin])) {
		// Si le plugin utilisateur permet la configuration du dossier des feeds ou ne suit pas la configuration
		// de Mashup Factory, il doit proposer un service pour fournir cette valeur.
		if ($initialiser = ezmashup_chercher_service($plugin, 'feed_initialiser_dossier')) {
			$dossiers[$plugin] = $initialiser();
		} else {
			// Le service ne propose pas de fonction propre, on utilise celle de Mashup Factory.
			$dossiers[$plugin] = 'feeds/';
		}
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $dossiers[$plugin];
}

/**
 * Trouve, pour un plugin utilisateur, les fichiers de configuration YAML des feeds éditables et non éditables.
 * Cherche les feeds non éditables dans le path (cas le plus fréquent) et dans _DIR_ETC pour les feeds éditables.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @uses ezmashup_feed_initialiser_dossier()
 *
 * @param string $plugin Préfixe du plugin utilisateur.
 *
 * @return array Liste des fichiers YAML trouvés
 */
function ezmashup_feed_rechercher_yaml(string $plugin) : array {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	$liste_fichiers = [];

	// On récupère la configuration du dossier relatif du plugin utilisateur où chercher les feeds à charger.
	// -- Ce dossier est utilisé pour tous les feeds du plugin utilisateur, éditables ou pas.
	$dossier_base_feeds = ezmashup_feed_initialiser_dossier($plugin);

	// Chercher les feeds non éditables selon le pattern en parcourant le path.
	// Les feeds sont en général dans les dossiers des plugins utilisateurs.
	foreach (creer_chemin() as $d) {
		$f = $d . $dossier_base_feeds;
		if (@is_dir($f)) {
			$liste = preg_files($f, 'config.yaml$', 10000 - count($liste_fichiers));
			foreach ($liste as $chemin) {
				$liste_fichiers[] = $chemin;
			}
		}
	}

	// Chercher les feeds éditables qui ne sont pas dans les dossiers d'un plugin utilisateur mais dans les dossiers
	// non accessibles du path.
	// La configuration de ces feeds est un cache géré par le plugin Cache Factory
	include_spip('inc/ezcache_cache');
	$filtres = [
		'sous_dossier' => $plugin . '/' . $dossier_base_feeds . '*/',
	];
	$descriptions = cache_repertorier('ezmashup', 'config', $filtres);
	if ($descriptions) {
		$liste_fichiers = array_merge(
			$liste_fichiers,
			array_keys($descriptions)
		);
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $liste_fichiers;
}

/**
 * Finalise le peuplement d'un feed uniquement si celui-ci s'est correctement déroulée.
 *
 * Le plugin Mashup Factory supprime les caches ezREST des feeds de façon à s'assurer que toute requête les concernant
 * remonte les informations à jour et cohérentes avec le contenu des feeds.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @uses cache_reponse_vider()
 * @uses ezmashup_chercher_service()
 *
 * @param string $plugin Préfixe du plugin utilisateur.
 * @param array  $feed   Configuration du feed
 *
 * @return void
 */
function ezmashup_feed_completer_peuplement(string $plugin, array $feed) : void {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Suppression des caches ezREST des feeds.
	include_spip('inc/ezrest_cache');
	cache_reponse_vider('ezmashup', '', 'feeds');

	// Si le plugin utilisateur, veut compléter le traitement par défaut de Mashup Factory,
	// il doit proposer un service idoine.
	if ($completer = ezmashup_chercher_service($plugin, 'feed_completer_peuplement')) {
		$completer($feed);
	}
	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
}

/**
 * Finalise le vidage d'un feed uniquement si celui-ci s'est correctement déroulé.
 *
 * Le plugin Mashup Factory ne fait rien de particulier actuellement.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @uses ezmashup_chercher_service()
 *
 * @param string $plugin Préfixe du plugin utilisateur.
 * @param array  $feed   Configuration du feed
 *
 * @return void
 */
function ezmashup_feed_completer_vidage(string $plugin, array $feed) : void {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Si le plugin utilisateur, veut compléter le traitement de vidage par défaut de Mashup Factory,
	// il doit proposer un service idoine.
	if ($completer = ezmashup_chercher_service($plugin, 'feed_completer_vidage')) {
		$completer($feed);
	}
	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
}

// -----------------------------------------------------------------------
// ------------------------- TARGET DATASETS -----------------------------
// -----------------------------------------------------------------------

/**
 * Renvoie la configuration par défaut du dossier relatif où stocker les datasets cible de type fichier.
 *
 * Le service de Mashup Factory considère que par défaut le dossier relatif des feeds est `targets/`.
 * Il est inclus dans l'arborescence _DIR_ETC/ezmashup/.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @uses ezmashup_chercher_service()
 *
 * @param string $plugin Préfixe du plugin utilisateur.
 *
 * @return string Chemin relatif du dossier où sont stockés les cibles de type fichier.
 */
function ezmashup_target_initialiser_dossier(string $plugin) : string {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Initialisation de la catégorie par défaut en statique pour améliorer les performances
	static $dossiers = [];

	if (!isset($dossiers[$plugin])) {
		// Si le plugin utilisateur permet la configuration du dossier des cibles de type fichier ou ne suit pas la configuration
		// de Mashup Factory, il doit proposer un service pour fournir cette valeur.
		if ($initialiser = ezmashup_chercher_service($plugin, 'target_initialiser_dossier')) {
			$dossiers[$plugin] = $initialiser();
		} else {
			// Le service ne propose pas de fonction propre, on utilise celle de Mashup Factory.
			$dossiers[$plugin] = 'targets/';
		}
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $dossiers[$plugin];
}

/**
 * Complète éventuellement le contenu de la consigne de peuplement d'un dataset cible.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @uses ezmashup_chercher_service()
 *
 * @param string $plugin          Préfixe du plugin utilisateur.
 * @param array  $consigne        Consigne initialisé par défaut par Mashup Factory
 * @param array  $enregistrements Liste des enregistrement du dataset cible
 * @param array  $feed            Configuration du feed
 *
 * @return array Consigne éventuellement mise à jour.
 */
function ezmashup_target_completer_consigne(string $plugin, array $consigne, array $enregistrements, array $feed) : array {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Si besoin, on appelle prioritairement une fonction spécifique au feed pour compléter la consigne du peuplement du dataset
	$options = [
		'prefixe' => $feed['feed_id'],
		'include' => $feed['include']
	];
	if ($consigner = ezmashup_chercher_service($plugin, 'target_completer_consigne', $options)) {
		$consigne = $consigner($consigne, $enregistrements, $feed);
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $consigne;
}

/**
 * Complète un record de la cible déjà rempli avec les champs basiques et statiques. Mashup Factory déroule :
 * - construction d'un champ `label` multilingue (avant ou après les traitements spécifiques au feed)
 * - traitements spécifiques au feed
 * - suppression des champs non utilisés dans le stockage de la cible.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @uses ezmashup_record_compiler_label()
 * @uses ezmashup_chercher_service()
 *
 * @param string $plugin         Préfixe du plugin utilisateur.
 * @param array  $enregistrement Enregistrement du dataset cible en cours de traitement
 * @param array  $feed           Configuration du feed
 *
 * @return array Enregistrement mis à jour.
 */
function ezmashup_record_completer(string $plugin, array $enregistrement, array $feed) : array {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Si la table possède un label multilangue, on le calcule à partir des champs du type label_xx
	// où xx est le code de langue utilisé par SPIP.
	// - mode pre-complétion
	if ($feed['mapping']['has_label_field'] === 'pre') {
		$enregistrement = ezmashup_record_compiler_label($enregistrement);
	}

	// Si besoin on appelle une fonction spécifique pour chaque enregistrement afin de le compléter.
	$options = [
		'prefixe' => $feed['feed_id'],
		'include' => $feed['include']
	];
	if ($completer = ezmashup_chercher_service($plugin, 'record_completer', $options)) {
		$enregistrement = $completer($enregistrement, $feed);
	}

	// - mode post-complétion
	if ($feed['mapping']['has_label_field'] === 'post') {
		$enregistrement = ezmashup_record_compiler_label($enregistrement);
	}

	// Maintenant que l'enregistrement est entièrement complété on peut supprimer les champs temporaires
	// qui ne sont pas à insérer dans la base (unused_fields)
	if (!empty($feed['mapping']['unused_fields'])) {
		foreach ($feed['mapping']['unused_fields'] as $_champ) {
			unset($enregistrement[$_champ]);
		}
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $enregistrement;
}

/**
 * Fusionne un record déjà rempli avec le record en cours de traitment possédant la même clé primaire dans la liste des records.
 *
 * Si aucun traitement particulier n'est proposé par le plugin utilisateur, Mashup Factory renvoie vide pour indiquer
 * l'erreur de doublon de clé primaire.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @uses ezmashup_chercher_service()
 *
 * @param string $plugin                  Préfixe du plugin utilisateur.
 * @param array  $enregistrement_en_cours Enregistrement du dataset cible en cours de constitution
 * @param array  $enregistrement_en_liste Enregistrement du dataset cible déjà dans la liste
 * @param array  $feed                    Configuration du feed
 *
 * @return array Item mis à jour
 */
function ezmashup_record_fusionner(string $plugin, array $enregistrement_en_cours, array $enregistrement_en_liste, array $feed) : array {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	$options = [
		'prefixe' => $feed['feed_id'],
		'include' => $feed['include']
	];
	if ($fusionner = ezmashup_chercher_service($plugin, 'record_fusionner', $options)) {
		$enregistrement = $fusionner($enregistrement_en_cours, $enregistrement_en_liste, $feed);
	} else {
		// Il n'existe pas de fonction de fusion, c'est donc une erreur (clé en doublon) : on renvoie un enregistrement
		// vide pour le signaler.
		$enregistrement = [];
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $enregistrement;
}

/**
 * Finalise la liste des records : tous les traitements automatiques et configurés ont été effectués.
 *
 * Par défaut, Mashup Factory calcule, si besoin, le champ profondeur pour les hiérarchies.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @uses ezmashup_chercher_service()
 *
 * @param string $plugin          Préfixe du plugin utilisateur.
 * @param array  $enregistrements Liste des enregistrement du dataset cible
 * @param array  $feed            Configuration du feed
 *
 * @return array Enregistrement mis à jour
 */
function ezmashup_record_list_completer(string $plugin, array $enregistrements, array $feed) : array {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Si la configuration intègre un calcul de profondeur (pour les arborescences de territoires par exemple)
	// on déclenche le traitement qui rajoute ce champ pour tous les enregistrements.
	if (!empty($feed['mapping']['depth_fields'])) {
		// Extraction de la configuration du champ désignant le parent
		$champ_parent = $feed['mapping']['depth_fields']['parent_field'];

		// On indexe la liste des enregistrements par le code unique de l'élément
		$enregistrements = array_column($enregistrements, null, $feed['mapping']['depth_fields']['parent_value']);

		foreach ($enregistrements as $_cle => $_enregistrement) {
			$profondeur = $feed['mapping']['depth_fields']['base_value'];
			if ($code_parent = $_enregistrement[$champ_parent]) {
				// Un parent existe, on remonte la parenté pour définir la profondeur
				while ($code_parent) {
					if (isset($enregistrements[$code_parent])) {
						// Le parent est du même type, on continue à remonter la parenté
						++$profondeur;
						$code_parent = $enregistrements[$code_parent][$champ_parent];
					} else {
						// Le parent n'est pas du même type, cela revient à être au top du type
						$code_parent = '';
					}
				}
			}
			$enregistrements[$_cle][$feed['mapping']['depth_fields']['name']] = $profondeur;
		}
	}

	// Si besoin on appelle une fonction spécifique pour la liste des enregistrements afin de la finaliser.
	$options = [
		'prefixe' => $feed['feed_id'],
		'include' => $feed['include']
	];
	if ($completer = ezmashup_chercher_service($plugin, 'record_list_completer', $options)) {
		$enregistrements = $completer($enregistrements, $feed);
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $enregistrements;
}

/**
 * Complète un texte sous forme de balise multi avec une liste de traductions.
 * Les nouvelles traductions, si elles sont non vides, sont soit ajoutées, soit écrasent la traduction courante.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @param string $multi_in    Chaine au format `<multi>`.
 * @param array  $traductions Tableau des traductions à ajouter (format [langue] = texte).
 *
 * @return string Chaine complétée au format `<multi>`.
 */
function ezmashup_record_completer_traduction(string $multi_in, array $traductions): string {
	// On initialise la sortie avec le texte multi fourni en entrée.
	$multi_out = $multi_in;

	// On décompose le texte multi d'entrée en tableau de traductions
	if ($traductions) {
		$traductions_out = [];
		if (preg_match(_EXTRAIRE_MULTI, $multi_in, $match)) {
			$contenu_multi = trim($match[1]);
			if ($contenu_multi) {
				if (
					include_spip('inc/filtres')
					and function_exists('extraire_trads')
				) {
					// Filtre extraire_trads avant spip 4.2
					$traductions_out = extraire_trads($contenu_multi);
				} else {
					// Depuis spip 4.2 il faut passer par le collecteur Multis
					// -- on crée un objet collecteur multis
					include_spip('src/Texte/Collecteur/Multis');
					$collecteurMultis = new Spip\Texte\Collecteur\Multis();
					// -- on collecte les 2 multi en reconstituant la balise et on extrait les traductions
					//    car on est sur que le contenu est non vide.
					$multis = $collecteurMultis->collecter($multi_in);
					$traductions_out = $multis[0]['trads'];
				}
			}
		} else {
			// La chaine n'est pas en multi, on considère que c'est une traduction par défaut
			$traductions_out[''] = $multi_in;
		}

		// On boucle sur les nouvelles traductions à ajouter ou à mettre à jour
		foreach ($traductions as $_langue => $_traduction) {
			// Si la traduction d'une langue est vide on ne l'ajoute ni ne la modifie
			if ($_traduction) {
				$traductions_out[$_langue] = $_traduction;
			}
		}

		// On reconstitue le texte multi
		$multi_out = '';
		foreach ($traductions_out as $_langue => $_traduction) {
			if ($_traduction) {
				$multi_out .= ($_langue ? '[' . $_langue . ']' : '') . trim($_traduction);
			}
		}
		$multi_out = "<multi>{$multi_out}</multi>";
	}

	return $multi_out;
}

/**
 * Calcule un label multilangue à partir d'un enregistrement possédant des champs nommés `label_<code_de_langue>`
 * ou les champs `label` et `language`.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @internal
 *
 * @param array $enregistrement Enregistrement du dataset cible.
 *
 * @return array Enregistrement mis à jour avec le champ label même si celui-ci est vide.
 */
function ezmashup_record_compiler_label(array $enregistrement) {
	// On détermine la méthode de calcul suivant les champs existant dans l'enregistrement
	$label = '';
	if (isset($enregistrement['language'], $enregistrement['label'])) {
		$label = "[{$enregistrement['language']}]" . $enregistrement['label'];
	} else {
		// Constituer un champ label à partir des champs label_xx de l'enregistrement si ils existent
		foreach ($enregistrement as $_champ => $_valeur) {
			if ($_valeur
			and (substr($_champ, 0, 6) == 'label_')
			and ($langue = str_replace('label_', '', $_champ))) {
				$label .= "[{$langue}]{$_valeur}";
			}
		}
	}

	// On finalise le multi
	$enregistrement['label'] = $label
		? "<multi>{$label}</multi>"
		: '';

	return $enregistrement;
}

// -----------------------------------------------------------------------
// ------------------------- SOURCE DATASETS -----------------------------
// -----------------------------------------------------------------------

/**
 * Complète un item de la source venant d'être extrait ou en vérifie la conformité.
 * Si le plugin utilisateur détecte que l'item n'est pas valide (condition propre au plugin), il peut renvoyer
 * vide, ce qui exclura l'item.
 *
 * Le plugin Mashup Factory ne fait rien par défaut.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @uses ezmashup_chercher_service()
 *
 * @param string $plugin Préfixe du plugin utilisateur.
 * @param array  $item   Item d'un dataset source
 * @param array  $feed   Configuration du feed
 *
 * @return array Item mis à jour
 */
function ezmashup_item_completer(string $plugin, array $item, array $feed) : array {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Si besoin on appelle une fonction spécifique pour chaque item source afin de le compléter, le modifier ou l'exclure
	$options = [
		'prefixe' => $feed['feed_id'],
		'include' => $feed['include']
	];
	if ($completer = ezmashup_chercher_service($plugin, 'item_completer', $options)) {
		$item = $completer($item, $feed);
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $item;
}

// -----------------------------------------------------------------------
// ---------------------- CATEGORIES & ACTIONS ---------------------------
// -----------------------------------------------------------------------

/**
 * Renvoie la liste des catégories et leur description.
 *
 * Le plugin Mashup Factory fournit une liste limitée à la catégorie d'identifiant `default`.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @uses ezmashup_chercher_service()
 *
 * @param string $plugin Préfixe du plugin utilisateur.
 *
 * @return array Liste des catégories et de leur description au format [id] = tableau de description avec le nom,
 *               la description et l'icone.
 */
function ezmashup_feed_categorie_lister(string $plugin) : array {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Initialisation des catégories en statique pour améliorer les performances
	static $categories = [];

	if (!isset($categories[$plugin])) {
		// Initialisation des catégories par défaut
		$categories[$plugin] = [
			'default' => [
				'name'        => '<:ezmashup:label_feed_category_default:>',
				'description' => '<:ezmashup:description_feed_category_default:>',
				'icon'        => 'ezmashup_category-24.svg'
			],
		];

		// Si le plugin appelant complète la liste des catégories ou la modifie, il doit fournir un service associé.
		if ($lister = ezmashup_chercher_service($plugin, 'feed_categorie_lister')) {
			$categories[$plugin] = $lister($categories[$plugin]);
		}
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $categories[$plugin];
}

/**
 * Renvoie l'identifiant de la catégorie par défaut d'un plugin utilisateur.
 *
 * Le plugin Mashup Factory choisit la première catégorie de la liste.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @uses ezmashup_chercher_service()
 *
 * @param string $plugin Préfixe du plugin utilisateur.
 *
 * @return string Identifiant de la catégorie par défaut.
 */
function ezmashup_feed_categorie_initialiser_defaut(string $plugin) : string {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Initialisation de la catégorie par défaut en statique pour améliorer les performances
	static $categorie_defaut = [];

	if (!isset($categorie_defaut[$plugin])) {
		// Si le plugin utilisateur permet la configuration du défaut des catégories ou ne suit pas la configuration
		// de Mashup Factory, il doit proposer un service pour fournir cette valeur.
		if ($initialiser = ezmashup_chercher_service($plugin, 'feed_categorie_initialiser_defaut')) {
			$categorie_defaut[$plugin] = $initialiser();
		} else {
			// On extrait la première catégorie de la liste fournie par le servie idoine
			$liste_categories = ezmashup_feed_categorie_lister($plugin);
			$categorie_defaut[$plugin] = array_key_first($liste_categories);
		}
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $categorie_defaut[$plugin];
}

/**
 * Complète la liste des actions personnalisables.
 *
 * Le plugin Mashup Factory fournit la liste des actions principales permettant d'agir sur les feeds éditables ou non.
 * Cette liste est en général suffisante mais elle peut être compléter voire modifiée par un plugin utilisateur : `charger`,
 * `peupler`, `vider`, `creer`, `editer`, `supprimer` et `voir`.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @uses ezmashup_chercher_service()
 *
 * @param string $plugin Préfixe du plugin utilisateur.
 *
 * @return array Liste des actions et de leur description au format [id] = tableau de description avec le nom,
 *               le type, l'icone, l'autorisation, la présence dans un menu, etc.
 */
function ezmashup_feed_action_completer_liste(string $plugin) : array {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	// Initialisation des actions en statique pour améliorer les performances
	static $actions = [];

	if (!isset($actions_plugin[$plugin])) {
		// Si le plugin appelant complète la liste des actions personnalisables, il doit utiliser le pipeline et
		// renvoyer les actions supplémentaires.
		$actions[$plugin] = [];
		$flux = [
			'args' => [
				'plugin'   => $plugin,
			],
			'data' => []
		];
		$actions_plugin = pipeline('feed_action_completer_liste', $flux);
		if ($actions_plugin) {
			$actions[$plugin] = $actions_plugin;
		}
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $actions[$plugin];
}

/**
 * Renvoie l'URL d'une action d'administration sur un feed nécessitant de se rendre sur une page donnée (formulaire).
 * Les actions d'administration sont actuellement limitée à peupler, vider, éditer ou supprimer mais
 * seules les actions de création et d'édition de feed nécessitent une redirection vers le formulaire idoine.
 *
 * Le plugin Mashup Factory ne fait rien par défaut.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @uses ezmashup_chercher_service()
 *
 * @param string $plugin     Préfixe du plugin utilisateur.
 * @param string $action     Action d'aministration venant d'être exécutée avec succès (`creer`, `editer`)
 * @param array  $feed       Description complète du feed si édition ou vide si création
 * @param string $page_admin URL de la page d'admin d'où provient l'action.
 *
 * @return string URL de traitement de l'action.
 */
function ezmashup_feed_action_definir_url(string $plugin, string $action, array $feed, string $page_admin) : string {
	//spip_log(' deb ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	$url = '';

	// Mashup Factory ne propose pas nativement un formulaire d'édition ou de création : aucune redirection n'est donc
	// nécessaire par défaut. Si le plugin utilisateur, propose un formulaire d'édition ou de création de feed,
	// il doit proposer une url idoine vers ce formulaire.
	if ($rediriger = ezmashup_chercher_service($plugin, 'feed_action_definir_url')) {
		$url = $rediriger($action, $feed, $page_admin);
	}

	//spip_log(' fin ' . __FUNCTION__, 'ezmashup_trace' . _LOG_DEBUG);
	return $url;
}

// ------------------------------------------------------------------
// ------------------------ UTILITAIRES  ----------------------------
// ------------------------------------------------------------------

/**
 * Normalise le chemin d'un include de fonctions spécifiques d'un feed.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @internal
 *
 * @param string      $include      Expression de l'include suivant la syntaxe brute du YAML
 * @param null|string $dossier_feed Dossier relatif des feeds où se trouve l'include ou vide si l'include est ailleurs
 *
 * @return string Chemin complet de l'include
 */
function ezmashup_normaliser_include(string $include, ?string $dossier_feed = '') {
	// Détermination du chemin exact de l'include
	// - '/nom_fichier' indique que le fichier est dans le répertoire relatif par défaut, soit 'ezmashup/'.
	// - 'nom_fichier' indique que le fichier est dans le répertoire relatif du feed, soit 'dossier_des_feeds/id_du_feed/'
	// - 'dir_relatif/nom_fichier' indique qu'il faut utiliser le chemin relatif fourni tel que.
	$nom_include = basename($include);
	if ($include === "/{$nom_include}") {
		$include = "ezmashup/{$nom_include}";
	} elseif ($include === "//{$nom_include}") {
		$include = "{$dossier_feed}{$nom_include}";
	}

	return $include;
}

/**
 * Compile les traductions d'un idiome.
 *
 * @param string $plugin Préfixe du plugin utilisateur.
 * @param string $idiome Idiome ou texte brut
 *
 * @return string Les traductions de l'idiome en multi ou le texte fourni en entrée si celui-ci n'est pas un idiome
 */
function ezmashup_normaliser_idiome(string $plugin, string $idiome) : string {
	// Si le texte n'est pas un idiome on le renvoie tel quel
	$retour = $idiome;

	if (
		$idiome
		and preg_match(_EXTRAIRE_IDIOME, $idiome, $matches)
		and $matches[1]
	) {
		// On détermine le module et le raccourci
		$module = $matches[1];
		$code = $matches[2];

		// Récupérer la liste des langues autorisées
		include_spip('inc/lang_liste');

		// On cherche tous les fichiers de langue destines a la traduction de l'idiome
		$multi = '';
		$dossier = constant('_DIR_PLUGIN_' . strtoupper($plugin));
		if ($fichiers_langue = preg_files($dossier, "/lang/{$module}_[a-z0-9_]+\.php$")) {
			foreach ($fichiers_langue as $_fichier_langue) {
				$nom_fichier = basename($_fichier_langue, '.php');
				$langue = str_replace("{$module}_", '', $nom_fichier);
				// Si la langue est reconnue, on extrait la traduction demandée
				if (isset($GLOBALS['codes_langues'][$langue])) {
					$GLOBALS['idx_lang'] = $langue;
					include("{$_fichier_langue}");
					if (isset($GLOBALS[$langue][$code])) {
						$multi .= "[{$langue}]" . $GLOBALS[$langue][$code];
					}
				}
			}
		}

		if ($multi) {
			$retour = "<multi>{$multi}</multi>";
		}
	}

	return $retour;
}

/**
 * Cherche une fonction donnée en se basant sur le feed ou à défaut sur le plugin utilisateur.
 * Si ni le feed ni le plugin ne fournissent la fonction demandée la chaîne vide est renvoyée.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @internal
 *
 * @param string     $plugin   Préfixe du plugin utilisateur.
 * @param string     $fonction Nom de la fonction à chercher.
 * @param null|array $options  Options de calcul de la fonction :
 *                             - string include : Include dans lequel chercher la fonction. Si vide, on cherche dans ezmashup/$plugin.
 *                             - string prefixe : préfixe de la fonction. Remplace $plugin comme préfixe
 *                             Ces options sont prioritaires pour la recherche : si la fonction n'est pas trouvée on recherche
 *                             alors la fonction au niveau du plugin
 *
 * @return string Nom complet de la fonction si trouvée ou chaine vide sinon.
 */
function ezmashup_chercher_service(string $plugin, string $fonction, ?array $options = []): string {
	$fonction_trouvee = '';

	// Eviter la réentrance si on demande le plugin Mashup Factory
	if ($plugin !== 'ezmashup') {
		// On inclut toujours le fichier des services personnalisés du plugin utilisateur
		$include_plugin = "ezmashup/{$plugin}";
		include_spip($include_plugin);
		if (
			!empty($options['include'])
			and ($options['include'] !== $include_plugin)
		) {
			include_spip($options['include']);
		}

		// On cherche prioritairement au niveau du feed, puis au niveau du plugin
		$fonctions_a_chercher = [];
		if (!empty($options['prefixe'])) {
			$fonctions_a_chercher[] = "{$options['prefixe']}_{$fonction}";
		}
		$fonctions_a_chercher[] = "{$plugin}_{$fonction}";
		foreach ($fonctions_a_chercher as $_fonction) {
			if (function_exists($_fonction)) {
				$fonction_trouvee = $_fonction;
				break;
			}
		}
	}

	return $fonction_trouvee;
}

/**
 * Construit le message de trace qui sera fourni à la fonction `spip_log`.
 *
 * @package SPIP\MASHUP\SERVICE
 *
 * @internal
 *
 * @param array $erreur Tableau de l'erreur, de l'avertissement ou de l'information à loger :
 *                      - int    level      : Niveau d'erreur de SPIP.
 *                      - string code       : identifiant de l'erreur qui coincide avec l'idiome du message
 *                      - array  parameters : paramètres pour le calcul du message
 *
 * @return string Message à tracer
 */
function ezmashup_log_creer_message(array $erreur): string {
	$texte = '';

	// Construction du message
	if (!empty($erreur['code'])) {
		// Le code d'erreur (non traduit)
		$texte .= strtoupper($erreur['code']);

		// Les paramètres
		include_spip('inc/texte_mini');
		$parametres = '';
		if (!empty($erreur['parameters'])) {
			foreach ($erreur['parameters'] as $_parametre => $_valeur) {
				$parametres .= ($parametres ? ' - ' : '')
					. $_parametre . '=' . couper($_valeur, 200, '...');
			}
		}

		// Le texte complet
		$texte .= ' : ' . rtrim($parametres);
	}

	return $texte;
}
