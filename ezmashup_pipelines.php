<?php
/**
 * Ce fichier contient l'ensemble des pipelines du plugin.
 * Mashup Factory déclare la collection feeds pour le plugin REST Factory.
 *
 * @package SPIP\EZMASHUP\REST
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclare les collections accessibles via l'API ezREST.
 * Par défaut, le plugin propose uniquement la liste des feeds disponibles.
 *
 * @pipeline liste_ezcollection
 *
 * @param array $collections Configuration des collections déjà déclarées.
 *
 * @return array Collections complétées.
**/
function ezmashup_liste_ezcollection(array $collections) :array {
	// Initialisation du tableau des collections
	if (!$collections) {
		$collections = [];
	}

	// La collection feeds permet de renvoyer la liste des feeds disponibles éventuellement filtrés.
	// -- les feeds sont considérés disponibles si ils sont actifs et peuplés.
	$collections['feeds'] = [
		'module' => 'ezmashup',
		'cache'  => [
			'type'  => 'ezrest',
			'duree' => 3600 * 12
		],
		'filtres' => [
			[
				'critere'         => 'categorie',
				'est_obligatoire' => false,
				'sans_condition'  => false,
				'champ_nom'       => 'category',
			],
			[
				'critere'         => 'plugin',
				'est_obligatoire' => false,
				'sans_condition'  => false,
			],
		],
	];

	return $collections;
}
