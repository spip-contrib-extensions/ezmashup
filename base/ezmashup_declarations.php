<?php
/**
 * Ce fichier contient les déclarations liées à la base de données.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclaration des informations tierces (alias, traitements, jointures, etc)
 * sur les tables de la base de données modifiées ou ajoutées par le plugin.
 *
 * Le plugin se contente de déclarer les alias des tables et quelques traitements.
 *
 * @pipeline declarer_tables_interfaces
 *
 * @param array $interface Tableau global des informations tierces sur les tables de la base de données
 *
 * @return array Tableau fourni en entrée et mis à jour avec les nouvelles informations
 */
function ezmashup_declarer_tables_interfaces(array $interface) : array {
	// Les tables : permet d'appeler une boucle avec le *type* de la table uniquement
	$interface['table_des_tables']['feeds'] = 'feeds';

	// Les traitements
	// - table spip_feeds : on desérialise les tableaux et on passe typo
	$interface['table_des_traitements']['TARGET_OPTIONS']['feeds'] = 'json_decode(%s, true)';
	$interface['table_des_traitements']['MAPPING']['feeds'] = 'json_decode(%s, true)';
	$interface['table_des_traitements']['SOURCES_BASIC']['feeds'] = 'json_decode(%s, true)';
	$interface['table_des_traitements']['SOURCES_ADDON']['feeds'] = 'json_decode(%s, true)';
	$interface['table_des_traitements']['TAGS']['feeds'] = 'json_decode(%s, true)';
	$interface['table_des_traitements']['TITLE']['feeds'] = 'typo(%s)';
	$interface['table_des_traitements']['DESCRIPTION']['feeds'] = 'typo(%s)';

	return $interface;
}

/**
 * Déclaration des nouvelles tables de la base de données propres au plugin.
 *
 * Le plugin déclare une nouvelle table :
 *
 * - `spip_feeds`, qui contient les éléments descriptifs des feeds du mashup.
 *
 * @pipeline declarer_tables_principales
 *
 * @param array $tables_principales Tableau global décrivant la structure des tables de la base de données
 *
 * @return array Tableau fourni en entrée et mis à jour avec les nouvelles déclarations
 */
function ezmashup_declarer_tables_principales(array $tables_principales) : array {
	// Table spip_feeds
	$feeds = [
		'plugin'         => "varchar(30) DEFAULT '' NOT NULL",          // Préfixe du plugin fournisseur
		'feed_id'        => "varchar(255) DEFAULT '' NOT NULL",         // Identifiant du feed
		'title'          => "text DEFAULT '' NOT NULL",                 // Nom littéral du feed
		'description'    => "text DEFAULT '' NOT NULL",                 // Description optionnelle du feed
		'target_format'  => "varchar(16) DEFAULT 'sql_table' NOT NULL", // Format de stockage de la cible, `sql_table` ou `file`
		'target_id'      => "varchar(255) DEFAULT '' NOT NULL",         // Nom de la table ou nom du fichier avec extension
		'target_options' => "text DEFAULT '' NOT NULL",                 // Tableau JSON des paramètres optionnels de la cible
		'mapping'        => "text DEFAULT '' NOT NULL",                 // Tableau JSON de la configuration du mapping
		'include'        => "varchar(255) DEFAULT '' NOT NULL",         // Chemin relatif du fichier contenant les fonctions spécifiques du feed
		'sources_basic'  => "text DEFAULT '' NOT NULL",                 // Tableau JSON des sources primaires
		'sources_addon'  => "text DEFAULT '' NOT NULL",                 // Tableau JSON des sources secondaires
		'sources_hash'   => "varchar(32) DEFAULT '' NOT NULL",          // MD5 des sources primaires et secondaires
		'category'       => "varchar(64) DEFAULT 'default' NOT NULL",   // Identifiant de la categorie du feed
		'tags'           => "text DEFAULT '' NOT NULL",                 // Tableau JSON des tags optionnels (couples nom=valeur)
		'hash'           => "varchar(32) DEFAULT '' NOT NULL",          // MD5 du feed calculé à partir du fichier config.yaml
		'is_active'      => "varchar(3) DEFAULT 'oui' NOT NULL",        // Indicateur d'activité du feed. Si différent de 'oui' (erreur), le feed ne peut pas être utilisé
		'is_editable'    => "varchar(3) DEFAULT 'non' NOT NULL",        // Indicateur d'éditabilité du feed.
		'maj'            => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp',
	];

	$feeds_cles = [
		'PRIMARY KEY'       => 'plugin, feed_id',
		'KEY target_format' => 'target_format',
		'KEY category'      => 'category',
		'KEY is_active'     => 'is_active',
		'KEY is_editable'   => 'is_editable',
	];

	$tables_principales['spip_feeds'] = [
		'field' => &$feeds,
		'key'   => &$feeds_cles,
	];

	return $tables_principales;
}
