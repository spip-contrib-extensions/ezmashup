<?php
/**
 * Ce fichier contient l'action `supprimer_feed` lancée par un utilisateur autorisé pour
 * supprimer un feed éditable.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet à l'utilisateur de supprimer, de façon sécurisée, un feed éditable.
 * Cela consiste à supprimer le dossier du feed et l'ensemble de ses ressources (config.yaml et fichiers sources éventuels).
 *
 * Les données du feed sont au préalable vidées et le feed lui-même est déchargé de la base.
 *
 * Cette action est réservée aux utilisateurs pouvant modifier un feed.
 * Elle nécessite l'id du feed et le préfixe du plugin utilisateur.
 *
 * @uses feed_supprimer()
 *
 * @param null|string $arguments Arguments de l'action ou null si l'action est appelée par une URL
 *
 * @return array Tableau de retour contenant l'erreur éventuelle
 * @throws Exception
 */
function action_supprimer_feed_dist(?string $arguments = null) : array {
	// Sécurisation.
	// Arguments attendus :
	// - l'identifiant du feed
	if (null === $arguments) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arguments = $securiser_action();
	}
	[$plugin, $id_feed] = explode(':', $arguments);

	// Verification des autorisations
	if (!autoriser('modifier', 'feed', $id_feed, null, ['plugin' => $plugin])) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// On lance le déchargement du feed en préalable
	include_spip('inc/ezmashup_feed');

	return feed_supprimer($plugin, $id_feed);
}
