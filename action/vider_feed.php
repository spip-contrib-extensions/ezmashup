<?php
/**
 * Ce fichier contient l'action `vider_feed` lancée par un utilisateur autorisé pour
 * vider les données du feed dans le dataset cible.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet à l'utilisateur de vider, de façon sécurisée, le dataset cible correspondant au feed.
 *
 * Cette action est réservée aux utilisateurs pouvant exécuter un feed.
 * Elle nécessite l'id du feed et le préfixe du plugin utilisateur.
 *
 * @uses feed_vider()
 *
 * @param null|string $arguments Arguments de l'action ou null si l'action est appelée par une URL
 *
 * @return array Tableau de retour contenant l'erreur éventuelle
 * @throws Exception
 */
function action_vider_feed_dist(?string $arguments = null) : array {
	// Sécurisation.
	// Arguments attendus :
	// - l'identifiant du feed
	if (null === $arguments) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arguments = $securiser_action();
	}
	[$plugin, $id_feed] = explode(':', $arguments);

	// Verification des autorisations
	if (!autoriser('executer', 'feed', $id_feed, null, ['plugin' => $plugin])) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// On lance la fonction gérant l'exécution du contrôle
	include_spip('inc/ezmashup_feed');

	return feed_vider($plugin, $id_feed);
}
