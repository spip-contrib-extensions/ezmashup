<?php
/**
 * Ce fichier contient l'action `recharger_feeds` lancée par un utilisateur pour
 * recharger, de façon sécurisée, la configuration des feeds d'un plugin utilisateur en base de données.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet à l'utilisateur de recharger en base de données, de façon sécurisée,
 * les feeds d'un plugin utilisateur donné à partir de leur fichier YAML.
 *
 * Cette action est réservée aux utilisateurs pouvant utiliser le plugin Mashup Factory.
 * Elle nécessite le préfixe du plugin utilisateur comme seul argument.
 *
 * @uses feed_charger()
 *
 * @param null|string $arguments Arguments de l'action ou null si l'action est appelée par une URL
 *
 * @return void
 */
function action_recharger_feeds_dist(?string $arguments = null) : void {
	// Sécurisation.
	// Arguments attendus :
	// - l'identifiant du feed
	if (null === $arguments) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arguments = $securiser_action();
	}
	$plugin = $arguments;

	// Verification des autorisations : pour recharger les types de contrôle il suffit
	// d'avoir l'autorisation minimale de Check Factory.
	if (!autoriser('charger', '_feeds', '', null, ['plugin' => $plugin])) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// Rechargement des types de contrôle : on force le recalcul complet, c'est le but.
	include_spip('inc/ezmashup_feed');
	feed_charger($plugin, true);
}
