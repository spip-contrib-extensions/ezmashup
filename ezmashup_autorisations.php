<?php
/**
 * Ce fichier contient les fonctions d'autorisations du plugin.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction appelée par le pipeline.
 */
function ezmashup_autoriser() {
}

/**
 * Autorisation minimale d'accès à toutes les pages du plugin Mashup Factory ou d'un plugin utilisateur.
 * Il faut :
 * - fournir un préfixe valide de plugin actif.
 * - éventuellement, avoir une autorisation d'accès spécifique (service propre au plugin)
 *   Par défaut, en l'absence de personnalisation par un plugin, Mashup Factory limite l'autorisation
 *   aux administrateurs complets.
 *
 * Cette autorisation est à la base des autres autorisations du plugin. Il n'est jamais utile de l'appeler dans
 * une fonction autre que les autorisations dérivées.
 *
 * @uses ezmashup_plugin_autoriser()
 *
 * @param string         $faire   L'action : `ezmashup`
 * @param string         $type    Le type d'objet ou nom de table : chaine vide
 * @param int            $id      Id de l'objet sur lequel on veut agir : 0, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `plugin`, préfixe du plugin utilisateur
*
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_ezmashup_dist($faire, $type, $id, $qui, $options) {
	// Initialisation de l'autorisation à non autorisé par défaut.
	$autorise = false;

	if (!empty($options['plugin'])
		and defined('_DIR_PLUGIN_' . strtoupper($options['plugin']))
		and include_spip('ezmashup/ezmashup')
	) {
		$autorise = ezmashup_plugin_autoriser($options['plugin'], $qui);
	}

	return $autorise;
}

/**
 * Autorisation de voir la liste des feeds (page feeds ou autre page spécifique d'un plugin utilisateur).
 * Il faut :
 * - posséder l'autorisation minimale `ezmashup`
 * - éventuellement, avoir une autorisation de visualisation des feeds spécifique (service propre au plugin).
 *
 * @uses ezmashup_plugin_autoriser()
 *
 * @param string         $faire   L'action : `voir`
 * @param string         $type    Le type d'objet ou nom de table : `_feeds` (ce n'est pas un objet au sens SPIP)
 * @param int            $id      Id de l'objet sur lequel on veut agir : 0, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `plugin`, préfixe du plugin utilisateur
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_feeds_voir_dist($faire, $type, $id, $qui, $options) {
	return
		autoriser('ezmashup', $type, $id, $qui, $options)
		and include_spip('ezmashup/ezmashup')
		and ezmashup_plugin_autoriser($options['plugin'], $qui, 'voir');
}

/**
 * Autorisation de charger ou décharger les feeds d'un plugin utilisateur dans la table `spip_feeds`.
 * Il faut :
 * - posséder l'autorisation minimale `ezmashup`.
 * - éventuellement, avoir une autorisation de chargement spécifique (service propre au plugin)
 *
 * @param string         $faire   Action demandée : `charger`
 * @param string         $type    Le type d'objet ou nom de table : `_feeds` (ce n'est pas un objet au sens SPIP)
 * @param int            $id      Identifiant de l'objet : `0`, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `plugin`, préfixe du plugin utilisateur
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_feeds_charger_dist($faire, $type, $id, $qui, $options) {
	return
		autoriser('ezmashup', $type, $id, $qui, $options)
		and include_spip('ezmashup/ezmashup')
		and ezmashup_plugin_autoriser($options['plugin'], $qui, 'charger');
}

/**
 * Autorisation de peupler ou de vider le dataset cible d'un feed.
 * Il faut :
 * - posséder l'autorisation minimale `ezmashup`
 * - fournir un identifiant de feed existant
 * - que le feed soit actif
 * - et éventuellement, avoir une autorisation d'exécution spécifique (service propre au plugin)
 *
 * @uses ezmashup_plugin_autoriser()
 *
 * @param string         $faire   Action demandée : `executer` (pour peupler, vider)
 * @param string         $type    Type d'objet sur lequel appliquer l'action : `feed`
 * @param int            $id      Identifiant de l'objet : celui du feed sur lequel appliquer l'action
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `plugin`, préfixe du plugin utilisateur
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_feed_executer_dist($faire, $type, $id, $qui, $options) {
	// Initialisation de l'autorisation à non autorisé par défaut.
	$autorise = false;

	if (
		autoriser('ezmashup', $type, $id, $qui, $options)
		and $id
		and is_string($id)
		and include_spip('inc/ezmashup_feed')
		and ($feed = feed_lire($options['plugin'], $id))
		and ($feed['is_active'] === 'oui')
		and include_spip('ezmashup/ezmashup')
		and ezmashup_plugin_autoriser($options['plugin'], $qui, 'executer', $feed)
	) {
		$autorise = true;
	}

	return $autorise;
}

/**
 * Autorisation de créer des feeds (page feeds ou autre page spécifique d'un plugin utilisateur).
 * Il faut :
 * - posséder l'autorisation minimale `ezmashup`.
 * - éventuellement, avoir une autorisation de création spécifique (service propre au plugin)
 *
 * Par défaut, Mashup Factory renvoie false pour cette autorisation.
 *
 * @uses ezmashup_plugin_autoriser()
 *
 * @param string         $faire   L'action : `creer`
 * @param string         $type    Le type d'objet ou nom de table : `feed` (ce n'est pas un objet au sens SPIP)
 * @param int            $id      Id de l'objet sur lequel on veut agir : 0, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `plugin`, préfixe du plugin utilisateur
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_feed_creer_dist($faire, $type, $id, $qui, $options) {
	return
		autoriser('ezmashup', $type, $id, $qui, $options)
		and include_spip('ezmashup/ezmashup')
		and ezmashup_plugin_autoriser($options['plugin'], $qui, 'creer');

}

/**
 * Autorisation, pour les feeds éditables, de supprimer ou d'éditer un feed.
 * Il faut :
 * - posséder l'autorisation minimale `ezmashup`
 * - fournir un identifiant de feed existant
 * - que le feed soit actif et éditable
 * - et éventuellement, avoir une autorisation de modification spécifique (service propre au plugin)
 *
 * Par défaut, Mashup Factory renvoie false pour cette autorisation.
 *
 * @uses ezmashup_plugin_autoriser()
 *
 * @param string         $faire   Action demandée : `modifier` (pour éditer ou supprimer)
 * @param string         $type    Type d'objet sur lequel appliquer l'action : `feed`
 * @param int            $id      Identifiant de l'objet : celui du feed sur lequel appliquer l'action
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `plugin`, préfixe du plugin utilisateur
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_feed_modifier_dist($faire, $type, $id, $qui, $options) {
	// Initialisation de l'autorisation à non autorisé par défaut.
	$autorise = false;

	if (
		autoriser('ezmashup', $type, $id, $qui, $options)
		and $id
		and is_string($id)
		and include_spip('inc/ezmashup_feed')
		and ($feed = feed_lire($options['plugin'], $id))
		and ($feed['is_active'] === 'oui')
		and ($feed['is_editable'] === 'oui')
		and include_spip('ezmashup/ezmashup')
		and ezmashup_plugin_autoriser($options['plugin'], $qui, 'modifier', $feed)
	) {
		$autorise = true;
	}

	return $autorise;
}
