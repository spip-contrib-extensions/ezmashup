<?php
/**
 * Ce fichier contient les fonctions de création, de mise à jour et de suppression
 * du schéma de données propre au plugin (tables et configuration).
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Installation du schéma de données propre au plugin et gestion des migrations suivant
 * les évolutions du schéma.
 *
 * Le schéma comprend des tables et des variables de configuration.
 *
 * @param string $nom_meta_base_version Nom de la meta dans laquelle sera rangée la version du schéma
 * @param string $version_cible         Version du schéma de données en fin d'upgrade
 *
 * @return void
 */
function ezmashup_upgrade(string $nom_meta_base_version, string $version_cible) : void {
	$maj = [];

	// Création des tables
	$maj['create'] = [
		['maj_tables', ['spip_feeds']],
	];

	$maj['2'] = [
		['sql_alter', "TABLE spip_feeds ADD is_editable varchar(3) DEFAULT 'non' NOT NULL AFTER is_active"],
		['sql_alter', 'TABLE spip_feeds ADD INDEX (is_editable)'],
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Suppression de l'ensemble du schéma de données propre au plugin, c'est-à-dire
 * les tables et les variables de configuration.
 *
 * @param string $nom_meta_base_version Nom de la meta dans laquelle sera rangée la version du schéma
 *
 * @return void
 */
function ezmashup_vider_tables(string $nom_meta_base_version) : void {
	// On efface la table de stockage des feeds
	sql_drop_table('spip_feeds');

	// on efface la meta du schéma du plugin
	effacer_meta($nom_meta_base_version);
}
