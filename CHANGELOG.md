# Changelog

## [Unreleased]

## [1.1.3] - 2024-08-20

### Added

- Ajout du guide conception du plugin

### Changed

- Actions qualité sur le code et le PHPDoc

## [1.1.2] - 2024-08-13

### Fixed

- Ne pas afficher les boutons d'exécution si le feed est invalide.
- Correction du test d'éditabilité

### Changed

- Mieux gérer le calcul des url d'administration et fournir la page de départ comme redirection possible.
- Mise au point de la définition de la page de traitement de l'action d'édition ou de création et renommage des fonctions associées pour plus de cohérence.

### Added

- Ajout d'une saisie de la liste des catégories

## [1.1.1] - 2024-07-27

### Changed

- Mise au point des autorisations

### Fixed

- Correction du chargement de l'indicateur d'éditabilité du feed

## [1.1.0] - 2024-07-20

### Added

- Mise au point des autorisations en ajoutant une possibilité de personnalisation.
- Ajout d'une balise pour calculer l'URL des pages créer et éditer.
- Extension de la liste des feeds pour permettre de traiter aussi les feeds éditables.

## [1.0.4] - 2024-07-01

### Added

- Ajout d'une API de lecture des ressources de feed éditable
- Ajout d'une saisie pour les plugins utilisant Mashup Factory
- Compatibilité SPIP 4

### Changed

- Le fichier d'options devien un fichier de fonctions

## [1.0.3] - 2024-01-27

### Changed

- Mise au point des fonctions sur les ressources en suivant l'évolution de Cache Factory

## [1.0.2] - 2024-01-21

### Added

- Prise en compte de la création de feed édiatble (autorisations, API de gestion des ressources, recherche des YAML, boutons d'action sur les feeds)
- Ajout des tags techniques

### Changed

- Prise en compte de la nouvelle version de Cache Factory (encodage, décodage exporté dans le plugin Encoder Factory)
- Mise à jour du template pour les nouvelles features

## [1.0.1] - 2023-12-23

### Added

- Ajout de la personnalisation des services par le feed et plus uniquement par le plugin

## [1.0.0] - 2023-05-28

Première version stable du plugin.
